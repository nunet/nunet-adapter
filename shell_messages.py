from dataclasses import dataclass
from ipv8.messaging.payload_dataclass import overwrite_dataclass

dataclass = overwrite_dataclass(dataclass)

@dataclass(msg_id=1)  # Important note: the message id is cleartext!
class DataPayload:
    content: bytes

@dataclass(msg_id=2)  # Important note: the message id is cleartext!
class PingRequest:
    content: bytes

@dataclass(msg_id=3)  # Important note: the message id is cleartext!
class PingReply:
    content: bytes

@dataclass(msg_id=4)  # Important note: the message id is cleartext!
class InitiateConnection:
    content: bytes

@dataclass(msg_id=5)  # Important note: the message id is cleartext!
class ConnectionRequest:
    content: bytes

@dataclass(msg_id=6)  # Important note: the message id is cleartext!
class ConnectionAccepted:
    content: bytes

@dataclass(msg_id=7)  # Important note: the message id is cleartext!
class SessionCreated:
    content: bytes

@dataclass(msg_id=8)  # Important note: the message id is cleartext!
class QueryRelay:
    content: bytes

@dataclass(msg_id=9)  # Important note: the message id is cleartext!
class QueryReply:
    content: bytes

@dataclass(msg_id=10)
class AckMsg:
    content: bytes