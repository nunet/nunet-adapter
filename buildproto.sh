python3 -m grpc_tools.protoc -I. --python_out=. --grpc_python_out=. service_spec/nunet_adapter.proto

python3 -m grpc_tools.protoc -I. --python_out=. --grpc_python_out=. service_spec/service_proto.proto

python3 -m grpc_tools.protoc -I. --python_out=. --grpc_python_out=. service_spec/tokenomics.proto

python3 -m grpc_tools.protoc -I. --python_out=. --grpc_python_out=. service_spec/service.proto

python3 -m grpc_tools.protoc -I. --python_out=. --grpc_python_out=. service_spec/stats_db.proto
