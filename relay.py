import service_pb2_grpc
import service_pb2
import os
import sys
from asyncio import ensure_future, get_event_loop

import libnacl

from ipv8.community import Community, DEFAULT_MAX_PEERS
from ipv8.configuration import ConfigBuilder, Strategy, WalkerDefinition, default_bootstrap_defs
from ipv8_service import IPv8
from ipv8.lazy_community import lazy_wrapper
import relay_messages
import time
import pickle
import grpc
import uuid
import utils
import log

from utils import Session

logger = log.setup_custom_logger('TRACE')

sys.path.append("./service_spec")

registry_address = "195.201.197.25:4859"


class NunetRelayCommunity(Community):

    community_id = b'\xc2\x98da\x0b\xa7LN\x8a(\xc1\xfcV\xf7m0\xc5s\xc8\x25'

    def __init__(self, my_peer, endpoint, network, max_peers=DEFAULT_MAX_PEERS, anonymize=False):
        super().__init__(my_peer, endpoint, network, max_peers, anonymize)
        self.known_peers = {}
        self.sessions = {}
        self.uid = uuid.uuid4()
        # self.start()
        # self.once = False
        self.add_message_handler(
            relay_messages.PingRequest, self.on_ping_request)
        self.add_message_handler(
            relay_messages.QueryRelay, self.on_query_relay)
        self.add_message_handler(
            relay_messages.InitiateConnection, self.on_initiate_connection)
        self.add_message_handler(
            relay_messages.ConnectionAccepted, self.on_connection_accepted)
        self.add_message_handler(
            relay_messages.DataPayload, self.on_datapayload)
        logger.info("Relay node started")
        logger.info(f"***{self.my_peer}***")
        self.register_task("query", self.query, interval=15.0, delay=0)

    @lazy_wrapper(relay_messages.PingRequest)
    async def on_ping_request(self, peer, payload):
        # logger.info(f"Received ping request from {peer} ")
        # logger.info(f"Known peers: {[peers.mid for peers in self.known_peers.keys()]}")
        update_time = time.time()
        self.known_peers[peer] = update_time
        self.ez_send(peer, relay_messages.PingReply(str.encode("Registered")))

    @lazy_wrapper(relay_messages.QueryRelay)
    async def on_query_relay(self, peer, payload):
        return_peer = peer
        # logger.info("*********************************")
        # logger.info(f"Received ping request from {peer} ")
        request = pickle.loads(payload.content)
        for peer in self.known_peers.keys():
            # logger.info(f"Requested mid {request['node']['mid']}")
            # logger.info(f"Peer mid {peer.mid}")
            if request['node']['mid'] == peer.mid:
                # logger.info("Found matching peer")
                # logger.info(f"Reply sent to {peer._address}")
                self.ez_send(
                    return_peer, relay_messages.QueryReply(str.encode('yes')))
        # logger.info("*********************************")

    @lazy_wrapper(relay_messages.InitiateConnection)
    async def on_initiate_connection(self, peer, payload):
        """[When requested to inititate connection the relay node
            stores the initiator as master and requests connection to the destination]
        """
        # logger.info(f"Received initiate connection from {peer} ")
        message = pickle.loads(payload.content)
        uid = message['uid']
        node = message['node']
        message['req_public_key'] = peer.public_key.key.pk.decode('ISO-8859-1')
        session = Session()
        session.master = peer
        for peer in self.known_peers.keys():
            if node['mid'] == peer.mid:
                session.slave = peer
        self.sessions[message['uid']] = session
        self.ez_send(session.slave, relay_messages.ConnectionRequest(
            pickle.dumps(message)))
        # Done until 4

    @lazy_wrapper(relay_messages.ConnectionAccepted)
    async def on_connection_accepted(self, peer, payload):
        # logger.info(f"Received connection accepted from {peer}")
        uid = pickle.loads(payload.content)
        session = self.sessions[uid]
        master = session.master
        slave = session.slave
        self.ez_send(master, relay_messages.SessionCreated(str.encode("Yes")))
        self.ez_send(slave, relay_messages.SessionCreated(str.encode("Yes")))

    @lazy_wrapper(relay_messages.DataPayload)
    async def on_datapayload(self, peer, payload):
        # logger.info(f"Received data payload from {peer}")

        msg = pickle.loads(payload.content)

        uid = msg['uid']
        session = self.sessions[uid]
        if msg['master']:
            # logger.info(f"command { msg['command']}")
            self.ez_send(
                session.slave, relay_messages.DataPayload(pickle.dumps(msg)))
        else:
            self.ez_send(session.master,
                         relay_messages.DataPayload(pickle.dumps(msg)))

    async def on_ackMsg(self, peer, payload):
        msg = pickle.loads(payload.content)

        uid = msg['uid']
        session = self.sessions[uid]
        self.ez_send(session.master, relay_messages.AckMsg(pickle.dumps(msg)))

    async def ping_relay(self):
        if self.get_peers():
            for peer in self.get_peers():
                self.ez_send(peer, relay_messages.PingRequest(
                    str.encode(time.time())))

    async def query(self):

        reg = {
            'peer_id': '',
            'ip_addrs': '',
            'services': [],
        }

        ip_peer = None

        service_desc = {}
        service_meta = {}
        peer_info = {}
        addrs = []
        service_meta['name'] = "relay"

        service_desc['name'] = "name"
        service_desc['endpoints'] = "addrs"
        service_meta['service_input'] = ''
        service_meta['service_output'] = ''
        service_meta['price'] = 0
        peer_info['nodeID'] = self.uid
        peer_info['key'] = self.my_peer.public_key.key_to_bin()
        peer_info['mid'] = self.my_peer.mid
        peer_info['public_key'] = self.my_peer.public_key.key_to_bin()
        peer_info['_address'] = self.my_peer._address
        ip_peer = self.my_peer._address
        peer_info['_address'] = ip_peer
        reg['peer_id'] = peer_info
        reg['ip_addrs'] = ["node_ip", "adapter_port"]
        reg['services'].append(service_meta)

        # Updates global orchestrator for automatic service deployment
        channel = grpc.insecure_channel(str(registry_address))
        stub = service_pb2_grpc.RegistryStub(channel)
        services = pickle.dumps(reg)
        try:
            if ip_peer != None and "UDPv4Address(ip='192" not in str(ip_peer) and "UDPv4Address(ip='127" not in str(ip_peer) and "UDPv4Address(ip='172" not in str(ip_peer) and "UDPv4Address(ip='10." not in str(ip_peer):
                response = stub.updateRegistry(
                    service_pb2.Services(services_info=services))
        except Exception as e:
            # logger.info(e)
            pass
