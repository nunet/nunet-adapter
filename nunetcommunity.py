import os
import sys
import subprocess
import json
import asyncio
from asyncio import ensure_future, get_event_loop

import libnacl
import log

logger = log.setup_custom_logger('TRACE')
from ipv8.community import Community, DEFAULT_MAX_PEERS
from ipv8.configuration import ConfigBuilder, Strategy, WalkerDefinition, default_bootstrap_defs
from ipv8_service import IPv8
from ipv8.lazy_community import lazy_wrapper
from shellsession import ShellSession
import shell_messages
import time
import uuid
import pickle
import grpc
import utils


class NunetCommunity(Community,ShellSession):

    community_id = b'\xc2\x98da\x0b\xa7LN\x8a(\xc1\xfcV\xf7m0\xc5s\xc8\x25'

    def __init__(self, my_peer, endpoint, network, max_peers=DEFAULT_MAX_PEERS, anonymize=False):
        super().__init__(my_peer, endpoint, network, max_peers, anonymize)
        self.symmetric_nat = False
        self.nat_checker()
        # self.queue = []
        self.response_times = {}
        self.relays = {}
        self.chosen_relay = None
        self.destination = None
        self.session_created = False
        self.received_response = False
        self.received_ack = False
        self.once = False
        
        self.uid = uuid.uuid4()
        self.add_message_handler(shell_messages.PingReply, self.on_ping_reply)
        self.add_message_handler(shell_messages.QueryReply, self.on_query_reply)
        self.add_message_handler(shell_messages.SessionCreated, self.on_session_created)
        self.add_message_handler(shell_messages.DataPayload, self.on_datapayload)
        self.add_message_handler(shell_messages.AckMsg, self.on_ackMsg)
        # self.register_task("query", self.query, interval=15.0, delay=0)
        if self.symmetric_nat:
            self.register_task("ping_relay", self.ping_relay, interval=5.0, delay=0)
        # self.register_task("start_session", self.start_session)

    def encrypt_message(self,content, peer_info):
        dest_peer = utils.create_peer(peer_info=peer_info)
        encrypted_message = libnacl.public.Box(self.my_peer.key.key.sk, dest_peer.public_key.key.pk).encrypt(content)

        return encrypted_message

    @lazy_wrapper(shell_messages.PingReply)   
    async def on_ping_reply(self, peer, payload):
        """[Receives the ping reply from the relay node and updates internal registry with response time]
        """ 
        if peer in self.relays.keys():
            received_time = time.time()
            response_time = received_time - self.relays[peer][0]
            self.relays[peer].append(response_time)
        else:
            self.relays[peer] = [time.time()]

    @lazy_wrapper(shell_messages.QueryReply)
    async def on_query_reply(self, peer, payload):
        # print("Received query reply from ", peer)
        if not self.chosen_relay:
            self.chosen_relay = peer
            # print("Chosen relay: ", self.chosen_relay._address)

     
    @lazy_wrapper(shell_messages.SessionCreated)
    async def on_session_created(self, peer, payload):
        self.session_created = True

    @lazy_wrapper(shell_messages.AckMsg)
    async def on_ackMsg(self, peer, payload):
        self.received_ack = True

    @lazy_wrapper(shell_messages.DataPayload)
    async def on_datapayload(self, peer, payload):
        msg = pickle.loads(payload.content)
        master = msg['master']
        if not master:
            output = msg['output']
        ShellSession.lock = 1
        ShellSession.response = output
        ShellSession.responded = True
        ShellSession.lock = 0

    async def ping_relay(self):
        """[Constantly pings the list of discovered relay nodes ]
        """ 
        endpoints = None
        if self.get_peers():
            '''Get relay peers automatically discovered by the community'''
            for peer in self.get_peers():
                self.ez_send(peer,shell_messages.PingRequest(str.encode(str(time.time()))))
        else:
            '''Get relay addrs from the global orchestrator'''
            try:
                endpoints = utils.get_endpoints("relay")
            except:
                print("Unable to find endpoints")
            if endpoints and endpoints["peer_ids"]:
                peer_ids = endpoints["peer_ids"]
                for peer_info in peer_ids:
                    peer = utils.create_peer(peer_info)
                    self.walk_to(peer._address)
                    self.ez_send(peer, shell_messages.PingRequest(str.encode(str(time.time()))))
    
    async def setup_connection_with_relay(self,peer_info):
        # logger.info("Setting up connection with relay...")
        endpoints = None
        if not self.relays.keys():
            # print("**No relay node discovered!!!**")
            while not self.relays.keys():
                # logger.info("No relay node discovered!!!")
                await asyncio.sleep(0.5)
     
        msg = {'uid': self.uid, 'node': peer_info}
        for peer in self.relays.keys():
            self.ez_send(peer,shell_messages.QueryRelay(pickle.dumps(msg)))
            # print("Sent to ", peer)

        while self.chosen_relay is None:
            await asyncio.sleep(1/100)
        # logger.info("Chosen relay: ", self.chosen_relay._address)
        self.ez_send(self.chosen_relay,shell_messages.InitiateConnection(pickle.dumps(msg)))
        count = 0
        # print("Sent initiate connection to ", self.chosen_relay._address)
        while not self.session_created:
            if count % 100 == 0:
                self.ez_send(self.chosen_relay,shell_messages.InitiateConnection(pickle.dumps(msg)))
            count += 1
            await asyncio.sleep(1/100)

    async def recreate_session(self,peer_info, payload):
        self.chosen_relay = None
        self.send(peer_info,payload)


    async def send(self,peer_info,payload):
        if self.symmetric_nat:
            if not self.chosen_relay:
                await self.setup_connection_with_relay(peer_info)
            self.ez_send(self.chosen_relay,shell_messages.DataPayload(pickle.dumps(payload)))
            ret = self.encrypt_message(content=str.encode("\r"), peer_info=self.destination)
            self.ez_send(self.chosen_relay,shell_messages.DataPayload(pickle.dumps({'uid': self.uid, 'master': True, 'command':ret})))
            # ret = str.encode("\r")
            # self.ez_send(self.chosen_relay,shell_messages.DataPayload(pickle.dumps({'uid': self.uid, 'master': True, 'command':ret})))
            
        else:
            peer = utils.create_peer(peer_info)
            self.ez_send(peer,shell_messages.DataPayload(pickle.dumps(payload)))
            ret = self.encrypt_message(content=str.encode("\r"), peer_info=self.destination)
            self.ez_send(peer,shell_messages.DataPayload(pickle.dumps({'uid': self.uid, 'master': True, 'command':ret})))
            # ret = str.encode("\r")
            # self.ez_send(peer,shell_messages.DataPayload(pickle.dumps({'uid': self.uid, 'master': True, 'command':ret})))

    def nat_checker(self):
        """Checks if the NAT is symmetric or not
        """      
        print("Detecting NAT...")  
        subprocess.call(["python2", "natdetector.py", "save"])
        with open('./nat-type.json', 'r') as file:
            meta = json.load(file)
            nat_type = meta['nat_type']
            print("**NAT type: ", nat_type)
        if "Symmetric" in nat_type:
            self.symmetric_nat = True
            