import pickle
import log
import os

logger = log.setup_custom_logger('TRACE')

device_name = os.environ['LS_SERVICE_NAME']

deployment_type = os.environ['deployment_type']

tokenomics_address = os.environ['tokenomics_address']

tokenomics_port_test = os.environ['tokenomics_port_test']

tokenomics_port_prod = os.environ['tokenomics_port_prod']

if deployment_type == "test":
    tokenomics_port = tokenomics_port_test

else:
    tokenomics_port = tokenomics_port_prod


class SharedRegistry:
    lock = 0
    nodeID = None
    has_gpu = None                       # Value to be set from the DMS through gRPC enpoint
    allow_cardano_deployment = None      # Value to be set from the DMS through gRPC enpoint
    gpu_info = {}                        # Value to be set from the DMS through gRPC enpoint
    
    machines_index = {

    }
    """
    Machines_index Schema
    key -> node_id
    value -> {
        "peer_info": {},
        "ip_addr": "",
        "available_resources":{},
        "tokenomics_address": "",
        "tokenomics_blockchain": "",
        "allow_cardano": "",
        "has_gpu": "",
        "gpu_info": "",
    }
    """
    
    available_resources_index = {

    }

    """
    Available_resources_index Schema
    key -> node_id
    value -> {
        "cpu_no": "",
        "cpu_hz": "",
        "price_cpu": "",
        "ram": "",
        "price_ram": "",
        "vcpu": "",
        "disk": "",
        "price_disk": ""
        
    }
    """

    services_index = {

    }
    
    """
    services_index Schema
    key -> node_id
    value -> {
            "service_id" : {
                "service_name" : "",
                "resoruce_requirements" : {
                    "cpu": "",
                    "ram": "",
                    "vcpu": "",
                    "hdd": ""
                }
                "container_type": "",
                "image_id": ""
            }
        }
    """

    @staticmethod
    def getServiceEndpoints(service_name):
        """[Returns the endpoints of the peers running the service with the given name.]

        Args:
            service_name ([String]): [The name of the service to query for in the registry.]

        Returns:
            [Dict]: [Dictionary of the addresses of the peers running the service]
        """        """"""
        try:
            address = {}
            peer_infos = []
            endpoints = []
            if "tokenomics" in service_name:
                address["endpoints"] = SharedRegistry.machines_index["tokenomics_address"]
                address["peer_ids"] = []
                # results = pickle.dumps(address)
                return address
            else:
                for node_id, service in enumerate(SharedRegistry.services_index):
                    if service_name in service["service_name"]:
                        endpoints.append(SharedRegistry.machines_index[node_id]["ip_addrs"])
                        peer_infos.append(SharedRegistry.machines_index[node_id]["peer_info"])
                address["peer_infos"] = peer_infos
                address["endpoints"] = endpoints
                # results = pickle.dumps(address)

                return address
        except Exception as e:
            logger.info(e)
