from ipv8.configuration import default_bootstrap_defs
from ipv8.dht.community import DHTCommunity
from ipv8.messaging.interfaces.udp.endpoint import UDPv4Address
from ipv8.lazy_community import lazy_wrapper
import ipv8_messages
import pickle
import time
import log
import os
from sharedRegistry import SharedRegistry
from shellsession import ShellSession
import utils
import uuid
import asyncio
from utils import *


logger = log.setup_custom_logger('TRACE')

device_name = os.environ['LS_SERVICE_NAME']

deployment_type = os.environ['deployment_type']

tokenomics_address = os.environ['tokenomics_address']

tokenomics_port_test = os.environ['tokenomics_port_test']

tokenomics_port_prod = os.environ['tokenomics_port_prod']


if deployment_type == "edge":
    tokenomics_port = tokenomics_port_test
    comm_id = b'\xc2\x98da\x0b\xa7LN\x8a(\xc1\xfcV\xf7m0\xc5s\xc2\x96'
elif deployment_type == "test":
    tokenomics_port = tokenomics_port_test
    comm_id = b'\xc2\x98da\x0b\xa7LN\x8a(\xc1\xfcV\xf7m0\xc5s\xc2\x97'
elif deployment_type == "team":
    tokenomics_port = tokenomics_port_test
    comm_id = b'\xc2\x98da\x0b\xa7LN\x8a(\xc1\xfcV\xf7m0\xc5s\xc2\x81'
elif deployment_type == "staging":
    tokenomics_port = tokenomics_port_test
    comm_id = b'\xc2\x98da\x0b\xa7LN\x8a(\xc1\xfcV\xf7m0\xc5s\xc2\x82'
else: #XXX can be used for production
    tokenomics_port = tokenomics_port_prod
    comm_id = b'\xc2\x98da\x0b\xa7LN\x8a(\xc1\xfcV\xf7m0\xc5s\xc2\x98'


class DiscoverPeerCommunity(DHTCommunity, SharedRegistry, ShellSession):
    community_id = comm_id

    def __init__(self, my_peer, endpoint, network, *args, **kwargs):
        super().__init__(my_peer, endpoint, network, *args, **kwargs)
        self.my_peer = my_peer
        self.network = network
        self.registry = {
            "node_ids": [],
            "peer_addrs": [],
            "peer_meta": [],
            "tokenomics": [str(tokenomics_address), str(tokenomics_port)],
        }
        self.lock = 0
        self.service_map = {
            'services': []
        }
        self.nodeID = uuid.uuid1()
        ShellSession.nodeID = self.nodeID
        SharedRegistry.nodeID = str(self.nodeID)
        self.add_message_handler(ipv8_messages.PeerInfo, self.peerInfo_handler)
        self.register_task("remove_outdated_peers",
                           self.remove_outdated_peers, interval=60.0, delay=0)

    @lazy_wrapper(ipv8_messages.PeerInfo)
    async def peerInfo_handler(self, peer, payload):
        """[Receives a PeerInfo message from a peer and adds it to the registry.
        The PeerInfo message contains the list of services running on that node.]

        Args:
            peer ([Peer]): [Peer object of the peer that sent the message.]
            payload ([type]): [The payload of the message.]
        """

        services_info = pickle.loads(payload.parameters)
        try:
            services_info["machine_index"]["timestamp"] = int(time.time())
            idx = services_info["machine_index"]["peer_info"]["nodeID"]
            if idx == str(self.nodeID):
                return
            if SharedRegistry.lock == 0:

                SharedRegistry.machines_index[idx] = services_info["machine_index"]
                SharedRegistry.available_resources_index[idx] = services_info["machine_index"]["available_resources"]
                SharedRegistry.services_index[idx] = services_info["services_index"]
                return
        except:
            # Added for compatability with old schema
            services_info["timestamp"] = int(time.time())
            if services_info["peer_id"]["nodeID"] == str(self.nodeID):
                return
            id = services_info["peer_id"]["nodeID"]
            machine_index = {}
            peer_info = {}
            peer_id = services_info["peer_id"]
            peer_info["nodeID"] = peer_id["nodeId"]
            peer_info["key"] = peer_id["key"]
            peer_info["mid"] = peer_id["mid"]
            peer_info["public_key"] = peer_id["public_key"]
            peer_info["_address"] = peer_id["_address"]
            machine_index["peer_info"] = peer_info
            machine_index["has_gpu"] = peer_id["has_gpu"]
            machine_index["gpu_info"] = peer_id["gpu_info"]
            machine_index["allow_cardano"] = peer_id["allow_cardano"]
            machine_index["timestamp"] = services_info["timestamp"]
            if SharedRegistry.lock == 0:

                SharedRegistry.machines_index[id] = services_info["peer_id"]
                SharedRegistry.available_resources_index[id] = {}
                SharedRegistry.services_index[id] = services_info["services"]
                return

    def remove_outdated_peers(self):
        """[Removes peers from the registry that have not been seen in the last minute.]"""
        SharedRegistry.lock = 1
        for id, machine in enumerate(SharedRegistry.machines_index):
            if id == str(self.nodeID):
                continue
            try:
                if (int(time.time()) - int(machine["timestamp"])) > 60:
                    # Remove machine metadata from DHT
                    del SharedRegistry.machines_index[id]
                    # Remove available resources information of the peer
                    del SharedRegistry.available_resources_index[id]
                    # Remove available services information of the peer
                    del SharedRegistry.services_index[id]
            except:
                continue
        SharedRegistry.lock = 0

    def dht_update(self):
        async def query():
            logger.info(f"Self NODEID -- {str(self.nodeID)}")
            logger.info(f"Number of Entries in DHT: {len(SharedRegistry.machines_index.keys())}")
            reg = {
                "machine_index": "",
                "services_index": ""
            }
            machine_index = {
                "peer_info": "",
                "ip_addr": "",
                "available_resources": "",
                "tokenomics_address": "",
                "tokenomics_blockchain": "",
                "has_gpu": "",
                "allow_cardano": "",
                "gpu_info": ""
            }
            peer_info = {}
            ip_peer = self.my_peer._address
            peer_info['nodeID'] = str(self.nodeID)
            peer_info['key'] = self.my_peer.public_key.key_to_bin()
            peer_info['mid'] = self.my_peer.mid
            peer_info['public_key'] = self.my_peer.public_key.key_to_bin()
            peer_info['_address'] = ip_peer

            machine_index["peer_info"] = peer_info
            machine_index["ip_addr"] = ip_peer
            try:
                machine_index["available_resources"] = SharedRegistry.available_resources_index[str(
                    self.nodeID)]
            except:
                machine_index["available_resources"] = {}
            machine_index["has_gpu"] = SharedRegistry.has_gpu
            machine_index["allow_cardano"] = SharedRegistry.allow_cardano_deployment
            machine_index["gpu_info"] = SharedRegistry.gpu_info
            # TODO Add tokenomics address and blockchain info
            SharedRegistry.machines_index[str(self.nodeID)] = machine_index

            reg["machine_index"] = machine_index
            try:
                reg["services_index"] = SharedRegistry.services_index[str(
                    self.nodeID)]
            except Exception:
                reg["services_index"] = {}

            try:
                if len(self.routing_tables) > 0:
                    if len(self.routing_tables[UDPv4Address].trie.root.value.nodes.values()) > 0:
                        for node in list(self.routing_tables[UDPv4Address].trie.root.value.nodes.values()):
                            encoded_result = encode_message(reg)
                            logger.info(node)
                            self.ez_send(
                                node, ipv8_messages.PeerInfo(encoded_result))

                else:
                    logger.info("Routing table is empty")
            except Exception:
                logger.info("Unable to parse routing table")

            # Updates global orchestrator for automatic service deployment
            channel = grpc.insecure_channel(str(registry_address))
            stub = service_pb2_grpc.RegistryStub(channel)
            services = encode_message(reg)
            try:
                if ip_peer != None and "UDPv4Address(ip='192" not in str(ip_peer) and "UDPv4Address(ip='127" not in str(ip_peer) and "UDPv4Address(ip='172" not in str(ip_peer) and "UDPv4Address(ip='10." not in str(ip_peer):
                    _ = stub.updateRegistry(
                        service_pb2.Services(services_info=services))
            except Exception:
                logger.error("Unable to communicate with global orchestrator")

        self.register_task("query", query, interval=15.0, delay=0)
