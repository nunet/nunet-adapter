class ShellSession:
    
    lock = 0
    command = None
    response = None
    started = False     # Flag to indicate whether Master Community has started 
    responded = False   # Flag to indicate if a response has been received 
    master_pkey = None  # Master Community peer's public key
    nodeID = None