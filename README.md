# Nunet Adapter

# Steps To Run

## Variables to be exported

```
    service_name
    service_address
    port

    db_name
    user_name
    password

    tokenomics_api_address
```

## Build

```
	docker build -t nunet_adapter /home/$USER/nunet-adapter/
```

## Start

```
    docker run -it --name=nunet-adapter-$service_name  -p $port:9998  \
    -e user_name=$user_name \
    -e db_name=$db_name \
    -e password=$password \
    -e tokenomics_api_name=$tokenomics_api_name \
    --network host \
    "nunet_adapter:latest"
```

# Test

## In a new terminal

```
docker exec -it nunet_adapter-$service_name bash
```

```
python3 test_nunet_adapter.py
```
