import sys
import grpc
from extract import *
import os
sys.path.append("./service_spec")
import nunet_adapter_pb2 as pb2
import nunet_adapter_pb2_grpc as pb2_grpc
import logging
import multiprocessing
import asyncio
import binascii

device_name = os.environ['device_name']


if len(sys.argv) == 2:
    grpc_port = sys.argv[1]
else:
    grpc_port="9998"

async def fake_news_call(channel):
    url="https://bbc.com"
    headline = extract_headline(url)
    body = extract_body(url)
    stub = pb2_grpc.NunetAdapterStub(channel)
    fp = pb2.ServiceDefnition()
    fp.service_name = "testing-uclnlp"
    fp.params = '{"body":"'+body+'","headline":"'+headline+'"}'
    result = await stub.reqService(fp)
    return str(result)

async def telemetry(channel):
    result="test"
    cpu_used=1.0
    memory_used=1.0
    net_used=1.0
    time_taken=1.0
    stub = pb2_grpc.NunetAdapterStub(channel)
    result=await stub.telemetry(pb2.TelemetryInput(result=result,cpu_used=cpu_used,memory_used=memory_used,net_used=net_used,time_taken=time_taken,device_name=device_name))
    if result!=None:
        logging.info("********result*******")
        logging.info(result)
        count = len(open("result.txt").readlines())
        print(count)
        with open("result.txt","a+") as f:
            f.write(str(count)+"\n")
    return str(result)

async def call_adapter(channel):
    url="https://bbc.com"
    headline = extract_headline(url)
    body = extract_body(url)
    stub = pb2_grpc.NunetAdapterStub(channel)
    fp = pb2.AdapterInput()
    fp.adapter_name = "testing-nunet-adapter-uclnlp"
    fp.service_name = "testing-uclnlp"
    fp.body = body
    fp.headline = headline
    result = await stub.callAdapter(fp)
    return result

async def make_transaction(channel):
    #sign
    stub = pb2_grpc.NunetAdapterStub(channel)
    fp = pb2.PaymentParams()
    fp.cost_per_process = "50"
    fp.pubk = "0xb5114121A51c6FfA04dBC73F26eDb7B6bfE2eB35"
    escrow_address = stub.signAdapterTransaction(fp)        
    escrow_address = escrow_address.escrow_address

    #make transaction
    stub = pb2_grpc.NunetAdapterStub(channel)
    fp = pb2.AdapterReleaseFunds()
    fp.escrow_address = escrow_address
    fp.public_key="0xb5114121A51c6FfA04dBC73F26eDb7B6bfE2eB35"
    fp.agi_amount="0.0000001"
    txn = await stub.makeAdapterTransaction(fp)        
    return txn.txn_hash

async def req_price(channel):
    adapterAddress="localhost:"+grpc_port
    channel = grpc.insecure_channel("{}".format(adapterAddress))
    stub = pb2_grpc.NunetAdapterStub(channel)
    fp = pb2.priceParams()
    fp.service_address = service_address
    result = await stub.reqPrice(fp)
    cost_per_process=result.cost_per_process
    pubk=result.pubk
    logging.info(result)
    return str(result)

async def req_service_metadata(channel):
    adapterAddress="localhost:"+grpc_port
    channel = grpc.insecure_channel("{}".format(adapterAddress))
    stub = pb2_grpc.NunetAdapterStub(channel)
    fp = pb2.metaServiceParams()
    fp.service_name = "testing-uclnlp"
    result = await stub.reqServiceMetadata(fp)
    logging.info(result)
    return str(result)

async def req_adapter_metadata(channel):
    adapterAddress="localhost:"+grpc_port
    channel = grpc.insecure_channel("{}".format(adapterAddress))
    stub = pb2_grpc.NunetAdapterStub(channel)
    fp = pb2.metaAdapterParams()
    fp.service_name = "testing-uclnlp"
    fp.adapter_name="testing-nunet-adapter-uclnlp"
    result = await stub.reqAdapterMetadata(iter((fp,)))
    print(list(result))
    logging.info(list(result))
    return result

async def req_agi_transfer(channel):
    adapterAddress="localhost:"+grpc_port
    channel = grpc.insecure_channel("{}".format(adapterAddress))
    stub = pb2_grpc.NunetAdapterStub(channel)
    fp = pb2.agiTransferParams()
    fp.public_key = "0xb5114121A51c6FfA04dBC73F26eDb7B6bfE2eB35"
    fp.amount="0.000000001"
    result = await stub.reqAdapterMetadata(fp)
    logging.info(result)   
    return result

async def get_peer_list(channel):
    adapterAddress="localhost:"+grpc_port
    channel = grpc.insecure_channel("{}".format(adapterAddress))
    stub = pb2_grpc.NunetAdapterStub(channel)
    fp = pb2.getPeerParams()
    result = stub.getPeerList(fp)
    print(result)
    return str(result)

async def get_self_peer(channel):
    adapterAddress="localhost:"+grpc_port
    channel = grpc.insecure_channel("{}".format(adapterAddress))
    stub = pb2_grpc.NunetAdapterStub(channel)
    fp = pb2.getPeerParams()
    result = stub.getSelfPeer(fp)
    print(result)
    return str(result)


async def get_dht_contents(channel):
    adapterAddress = "localhost:"+grpc_port
    channel = grpc.insecure_channel("{}".format(adapterAddress))
    stub = pb2_grpc.NunetAdapterStub(channel)
    fp = pb2.getDhtParams()
    result = stub.getDhtContent(fp)
    print(f"Machines Index {result.machines_index}")
    print(f"Available Resources Index {result.available_resources_index}")
    print(f"Services Index {result.services_index}")
    return str(result)

async def send_message(channel):
    
    adapterAddress = "localhost:"+grpc_port
    channel = grpc.insecure_channel("{}".format(adapterAddress))
    stub = pb2_grpc.NunetAdapterStub(channel)
    
    #get dht
    fp = pb2.getDhtParams()
    result = stub.getDhtContent(fp)
    node_ids = json.loads(result.machines_index)
    node_id = list(node_ids.keys())[0]
    print(node_id)

    #send message
    fp = pb2.messageParams()
    fp.message_content = "sample message"
    fp.node_id = node_id
    result = stub.sendMessage(fp)
    print(result)
    return str(result)


async def received_message(channel):
    adapterAddress = "localhost:"+grpc_port
    channel = grpc.insecure_channel("{}".format(adapterAddress))
    stub = pb2_grpc.NunetAdapterStub(channel)
    fp = pb2.receivedMessageParams()
    result = stub.receivedMessage(fp)
    print(result)
    return str(result)


async def updateDHT(channel):
    adapterAddress="localhost:"+grpc_port
    channel = grpc.insecure_channel("{}".format(adapterAddress))
    stub = pb2_grpc.NunetAdapterStub(channel)
    fp = pb2.DHTUpdateContent()
    fp.allow_cardano = json.dumps("False")
    fp.has_gpu = json.dumps("True")
    fp.gpu_info = json.dumps({
            "name": "NVIDIA RTX 2060",
            "vram": "6GB"
        })
    fp.services_index = json.dumps({
        "001": {
            "service_name": "news-score",
            "resource_requriements": {},
            "container_type": "",
            "image_id": ""
        }
    })
    fp.available_resources = json.dumps({
        "cpu_no": 6,
        "cpu_hz": 2100,
        "price_cpu": "",
        "ram": 4,
        "price_ram": "",
        "vcpu": "",
        "disk": "",
        "price_disk": ""
    })
    resp = stub.updateDHT(fp)
    print(resp)
    return str(resp)

async def execute_command(channel):
    adapterAddress = "localhost:"+grpc_port
    channel = grpc.insecure_channel("{}".format(adapterAddress))
    stub = pb2_grpc.NunetAdapterStub(channel)
    fp = pb2.Command(command="start")
    result = stub.executeCommand(fp)
    print(result.response)
    return str(result.response)

async def get_master_pkey(channel):
    adapterAddress = "localhost:"+grpc_port
    channel = grpc.insecure_channel("{}".format(adapterAddress))
    stub = pb2_grpc.NunetAdapterStub(channel)
    fp = pb2.getPkey()
    result = stub.getMasterPkey(fp)
    print(f"HEX --- {result.public_key}")
    print(binascii.unhexlify(result.public_key))
    return str(binascii.unhexlify(result.public_key))


async def concurrent_test():
    n=10
    with open("result.txt","w") as f:
        f.write("result\n")
    async with grpc.aio.insecure_channel('localhost:'+str(grpc_port)) as channel:
        processes = [multiprocessing.Process(target=telemetry,args=(channel,)) for x in range(int(n))]
        for p in processes:
            p.start()
async def main():
    #async with grpc.aio.insecure_channel('localhost:'+str(grpc_port)) as channel:
    #    await fake_news_call(channel)

    #async with grpc.aio.insecure_channel('localhost:'+str(grpc_port)) as channel:
    #    await telemetry(channel)

    #async with grpc.aio.insecure_channel('localhost:'+str(grpc_port)) as channel:
    #    await call_adapter(channel)

    #async with grpc.aio.insecure_channel('localhost:'+str(grpc_port)) as channel:
    #    await make_transaction(channel)

    #async with grpc.aio.insecure_channel('localhost:'+str(grpc_port)) as channel:
    #    await req_price(channel)

    #async with grpc.aio.insecure_channel('localhost:'+str(grpc_port)) as channel:
    #    await req_service_metadata(channel)

    #async with grpc.aio.insecure_channel('localhost:'+str(grpc_port)) as channel:
    #    await req_adapter_metadata(channel)
    
    #async with grpc.aio.insecure_channel('localhost:'+str(grpc_port)) as channel:
    #    await get_peer_list(channel)

    # async with grpc.aio.insecure_channel('localhost:'+str(grpc_port)) as channel:
    #     await get_self_peer(channel)

    # async with grpc.aio.insecure_channel('localhost:'+str(grpc_port)) as channel:
    #     await get_dht_contents(channel)

    # async with grpc.aio.insecure_channel('localhost:'+str(grpc_port)) as channel:
    #     await send_message(channel)

    # async with grpc.aio.insecure_channel('localhost:'+str(grpc_port)) as channel:
    #     await received_message(channel)

    # async with grpc.aio.insecure_channel('localhost:'+str(grpc_port)) as channel:
    #     await updateDHT(channel)

    # async with grpc.aio.insecure_channel('localhost:'+str(grpc_port)) as channel:
    #     await execute_command(channel)

    async with grpc.aio.insecure_channel('localhost:'+str(grpc_port)) as channel:
        await get_master_pkey(channel)
    #await concurrent_test()
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
