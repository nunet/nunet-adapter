import sys
import grpc
import os
sys.path.append("./service_spec")
import nunet_adapter_pb2 as pb2
import nunet_adapter_pb2_grpc as pb2_grpc
import logging
import multiprocessing
import asyncio

grpc_port=sys.argv[1]

def get_dht_contents():
    adapterAddress = f"localhost:{grpc_port}"
    channel = grpc.insecure_channel("{}".format(adapterAddress))
    stub = pb2_grpc.NunetAdapterStub(channel)
    fp = pb2.getDhtParams()
    result = stub.getDhtContent(fp)
    print(f"Machines Index {result.machines_index}")
    return result

def get_self_peer():
    adapterAddress = f"localhost:{grpc_port}"
    channel = grpc.insecure_channel("{}".format(adapterAddress))
    stub = pb2_grpc.NunetAdapterStub(channel)
    fp = pb2.getPeerParams()
    result = stub.getSelfPeer(fp)
    return result

def get_peer_list():
    adapterAddress = f"localhost:{grpc_port}"
    channel = grpc.insecure_channel("{}".format(adapterAddress))
    stub = pb2_grpc.NunetAdapterStub(channel)
    fp = pb2.getPeerParams()
    result = stub.getPeerList(fp)
    return result
    
def send_message(node_id, message):
    adapterAddress = f"localhost:{grpc_port}"
    channel = grpc.insecure_channel("{}".format(adapterAddress))
    stub = pb2_grpc.NunetAdapterStub(channel)
    fp = pb2.messageParams()
    fp.node_id = node_id
    fp.message_content = message
    result = stub.sendMessage(fp)
    return str(result)
    
def received_message():
    adapterAddress = f"localhost:{grpc_port}"
    channel = grpc.insecure_channel("{}".format(adapterAddress))
    stub = pb2_grpc.NunetAdapterStub(channel)
    fp = pb2.receivedMessageParams()
    result = stub.receivedMessage(fp)
    return str(result)