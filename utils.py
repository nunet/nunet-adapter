import sys
import grpc
import pickle
import os 

sys.path.append("./service_spec")
import service_pb2
import service_pb2_grpc


from ipv8.peer import Peer
from ipv8.keyvault.public.libnaclkey import LibNaCLPK

deployment_type = os.environ['deployment_type']

registry_address_prod = os.environ['registry_address_prod']
registry_address_test = os.environ['registry_address_test']


if deployment_type=="prod":
    registry_address=registry_address_prod
else:
    registry_address=registry_address_test
registry_address_rms = "195.201.197.25:4859"

def get_service_address(service_map, service_name):
    for service in service_map['services']:
        if service_name in service['name']:
            endpoint = service['endpoints'][0]
            service_address = 'localhost'
            service_port = endpoint[1]
            return str(service_address), str(service_port)

    return Exception


def get_address(service_name):
    with grpc.insecure_channel(str(registry_address)) as channel:
        stub = service_pb2_grpc.RegistryStub(channel)
        response = stub.reqServiceEndpoints(
            service_pb2.ServiceRequest(service_name=service_name))
        endpoints = pickle.loads(response.endpoints)
        peer_ids = endpoints["peer_ids"]
        ip_addrs = endpoints["endpoints"]
        if len(peer_ids) > 0:
            peer_info = peer_ids[len(peer_ids)-1]
            key = LibNaCLPK(peer_info['key'])
            peer = Peer(key)
            peer.mid = peer_info['mid']
            peer.public_key = peer_info['public_key']
            peer._address = peer_info['_address']
        else:
            peer = ""
        address = ip_addrs[0][0]
        port = ip_addrs[0][1]
        if "tokenomics" in service_name:
            address = ip_addrs[0]
            port = ip_addrs[1]
    return peer, (str(address), str(port))


def parse_nodeID(line):
    line = line.replace("\n", "")
    line = line.replace(" ", "")
    line = line.split("=")
    return line[1]


def encode_message(data):
    return pickle.dumps(data)


def decode_message(payload):
    return pickle.loads(payload)

def get_endpoints(service_name):
    with grpc.insecure_channel(str(registry_address)) as channel:
        stub = service_pb2_grpc.RegistryStub(channel)
        response = stub.reqServiceEndpoints(
            service_pb2.ServiceRequest(service_name=service_name))
        endpoints = pickle.loads(response.endpoints)

    return endpoints

def get_endpoint_rms(node_id):
    with grpc.insecure_channel(str(registry_address_rms)) as channel:
        stub = service_pb2_grpc.RegistryStub(channel)
        response = stub.reqServiceEndpoints(
            service_pb2.ServiceRequest(service_name=node_id))
        endpoints = pickle.loads(response.endpoints)

    return endpoints

def create_peer(peer_info):
    peer = Peer(peer_info['key'])
    peer._address = peer_info['_address']
    return peer

class Session:
    def __init__(self):
        self.master = None
        self.slave = None
