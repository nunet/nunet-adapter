# pylint: disable=import-outside-toplevel
import inspect
import sys

from ipv8.loader import CommunityLauncher,  kwargs, overlay,  set_in_session, walk_strategy
from ipv8.peer import Peer
from ipv8.keyvault.crypto import default_eccrypto
from ipv8.peerdiscovery.discovery import RandomWalk


INFINITE = -1
"""
The amount of target_peers for a walk_strategy definition to never stop.
"""


class IPv8CommunityLauncher(CommunityLauncher):
    def prepare(self, overlay_provider, session):
        """
        Perform setup tasks before the community is loaded.

        :type overlay_provider: ipv8.IPv8
        :type session: object
        """
        print("in prepare")

    def get_overlay_class(self):
        """
        Get the overlay class this launcher wants to load.

        This raises a RuntimeError if it was not overwritten at runtime, to appease Pylint.

        :rtype: ipv8.overlay.Overlay
        """
        print("in get_overlay_class")
        from nunet_adapter import NunetAdapter
        return NunetAdapter

    def get_walk_strategies(self):
        """
        Get walk strategies for this class.
        It should be provided as a list of tuples with the class, kwargs and maximum number of peers.
        """
        # return [(RandomWalk,{'timeout': 3.0}, 10),(PeriodicSimilarity, {},INFINITE),(RandomChurn,{},INFINITE)]
        return [(RandomWalk,{'timeout': 3.0}, 10)]

    def get_my_peer(self, ipv8, session):
        return Peer(default_eccrypto.generate_key("curve25519"))

    def get_bootstrappers(self, session):
        from ipv8.bootstrapping.dispersy.bootstrapper import DispersyBootstrapper
        from ipv8.configuration import DISPERSY_BOOTSTRAPPER

        return [(DispersyBootstrapper, DISPERSY_BOOTSTRAPPER['init'])]
   
# class MasterCommunityLauncher(CommunityLauncher):
#     def prepare(self, overlay_provider, session):
#         """
#         Perform setup tasks before the community is loaded.

#         :type overlay_provider: ipv8.IPv8
#         :type session: object
#         """
#         # print("in prepare")

#     def get_overlay_class(self):
#         """
#         Get the overlay class this launcher wants to load.

#         This raises a RuntimeError if it was not overwritten at runtime, to appease Pylint.

#         :rtype: ipv8.overlay.Overlay
#         """
#         # print("in get_overlay_class")
#         from master import MasterCommunity
#         return MasterCommunity

#     def get_walk_strategies(self):
#         """
#         Get walk strategies for this class.
#         It should be provided as a list of tuples with the class, kwargs and maximum number of peers.
#         """
#         # return [(RandomWalk,{'timeout': 3.0}, 10),(PeriodicSimilarity, {},INFINITE),(RandomChurn,{},INFINITE)]
#         return [(RandomWalk,{'timeout': 3.0}, 10)]

#     def get_my_peer(self, ipv8, session):
#         return Peer(default_eccrypto.generate_key("curve25519"))

#     def get_bootstrappers(self, session):
#         from ipv8.bootstrapping.dispersy.bootstrapper import DispersyBootstrapper
#         from ipv8.configuration import DISPERSY_BOOTSTRAPPER

#         return [(DispersyBootstrapper, DISPERSY_BOOTSTRAPPER['init'])]

class TestnetMixIn:
    def should_launch(self, session):
        return True


# communities
def discovery_community():
    from ipv8.peerdiscovery.community import DiscoveryCommunity
    return DiscoveryCommunity

def nunetadapter_community():
    from nunet_adapter import NunetAdapter
    return NunetAdapter

def discoverpeer_community():
    from discovery import DiscoverPeerCommunity
    return DiscoverPeerCommunity

def dht_discovery_community():
    from ipv8.dht.discovery import DHTDiscoveryCommunity
    return DHTDiscoveryCommunity

def nunet_relay_community():
    from relay import NunetRelayCommunity
    return NunetRelayCommunity

def master_community():
    from master import MasterCommunity
    return MasterCommunity


# strategies
def random_churn():
    from ipv8.peerdiscovery.churn import RandomChurn
    return RandomChurn


def ping_churn():
    from ipv8.dht.churn import PingChurn
    return PingChurn


def random_walk():
    from ipv8.peerdiscovery.discovery import RandomWalk
    return RandomWalk


def periodic_similarity():
    from ipv8.peerdiscovery.community import PeriodicSimilarity
    return PeriodicSimilarity


@overlay(discovery_community)
@kwargs(max_peers='100')
@walk_strategy(random_churn, target_peers=INFINITE)
@walk_strategy(random_walk)
@walk_strategy(periodic_similarity, target_peers=INFINITE)
class IPv8DiscoveryCommunityLauncher(IPv8CommunityLauncher):
    pass


@set_in_session('dht_community')
@overlay(dht_discovery_community)
@kwargs(max_peers='60')
@walk_strategy(ping_churn, target_peers=INFINITE)
@walk_strategy(random_walk)
class DHTCommunityLauncher(IPv8CommunityLauncher):
    pass

@set_in_session('discoverpeer_community')
@overlay(discoverpeer_community)
@kwargs(max_peers='60')
@walk_strategy(ping_churn, target_peers=INFINITE)
# @walk_strategy(random_walk)
class DiscoverPeerCommunityLauncher(IPv8CommunityLauncher):
    pass

@set_in_session('nunet_community')
# @overlay(nunetadapter_community)
@kwargs(max_peers='100')
# @walk_strategy(random_churn, target_peers=INFINITE)
@walk_strategy(random_walk)
# @walk_strategy(periodic_similarity, target_peers=INFINITE)
class NunetCommunityLauncher(IPv8CommunityLauncher):
    pass

# @set_in_session('nunet_community')
@overlay(nunet_relay_community)
@kwargs(max_peers='100')
# @walk_strategy(random_churn, target_peers=INFINITE)
@walk_strategy(random_walk)
# @walk_strategy(periodic_similarity, target_peers=INFINITE)
class NunetRelayCommunityLauncher(IPv8CommunityLauncher):
    pass


@set_in_session('master_community')
@overlay(master_community)
@kwargs(max_peers='100')
@walk_strategy(random_walk)
class MasterCommunityLauncher(IPv8CommunityLauncher):
    pass


def get_hiddenimports():
    """
    Return the set of all hidden imports defined by all CommunityLaunchers in this file.
    """
    hiddenimports = set()

    for _, obj in inspect.getmembers(sys.modules[__name__]):
        hiddenimports.update(getattr(obj, "hiddenimports", set()))

    return hiddenimports


def register_default_launchers(loader,nat_type):
    """
    Register the default CommunityLaunchers into the given CommunityLoader.
    If you define a new default CommunityLauncher, add it here.
    """
    loader.set_launcher(NunetCommunityLauncher())
    loader.set_launcher(DiscoverPeerCommunityLauncher())
    loader.set_launcher(MasterCommunityLauncher())
    if "Symmetric" not in nat_type:
        loader.set_launcher(NunetRelayCommunityLauncher())
   
    
