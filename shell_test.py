import sys
import grpc
import os
sys.path.append("./service_spec")
import nunet_adapter_pb2 as pb2
import nunet_adapter_pb2_grpc as pb2_grpc
from concurrent.futures import ThreadPoolExecutor
import logging
import multiprocessing
import asyncio

if len(sys.argv) == 2:
    grpc_port = sys.argv[1]
else:
    grpc_port="9998"


async def execute_command(channel, command):
    adapterAddress = "localhost:"+grpc_port
    channel = grpc.insecure_channel("{}".format(adapterAddress))
    stub = pb2_grpc.NunetAdapterStub(channel)
    fp = pb2.Command(command=command)
    result = stub.executeCommand(fp)
    print(bytes(str(result.response),"utf-8").decode("unicode_escape"),flush=True)

async def ainput():
        with ThreadPoolExecutor(1, "") as executor:
            return await asyncio.get_event_loop().run_in_executor(executor, input)

async def main():
    while True:
        print("> ",end="")
        command = await ainput()
        async with grpc.aio.insecure_channel('localhost:'+str(grpc_port)) as channel:
            response = await execute_command(channel, command)

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
