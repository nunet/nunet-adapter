import shell_messages
import sys
import utils
import grpc
import pickle
import asyncio
import libnacl
import log
from concurrent.futures import ThreadPoolExecutor
from ipv8.community import DEFAULT_MAX_PEERS
from nunetcommunity import NunetCommunity
from shellsession import ShellSession

logger = log.setup_custom_logger('TRACE')
sys.path.append("./service_spec")
import service_pb2_grpc
import service_pb2
registry_address_rms = "195.201.197.25:4859"


class MasterCommunity(NunetCommunity, ShellSession):

    community_id = b'\xc2\x98da\x0b\xa7LN\x8a(\xc1\xfcV\xf7m0\xc5s\xc8\x25'

    def __init__(self, my_peer, endpoint, network, max_peers=DEFAULT_MAX_PEERS, anonymize=False):
        super().__init__(my_peer, endpoint, network, max_peers, anonymize)
        # self.register_task("query", self.query, interval=15.0, delay=0)
        self.lastinput = ''
        self.started = False
        self.register_task("shell_manager",
                           self.shell_manager, interval=0.5, delay=0)
        ShellSession.master_pkey = self.my_peer.public_key.key.pk

    def encrypt_message(self, content, peer_info):
        dest_peer = utils.create_peer(peer_info=peer_info)
        encrypted_message = libnacl.public.Box(
            self.my_peer.key.key.sk, dest_peer.public_key.key.pk).encrypt(content)

        return encrypted_message

    def shell_manager(self):
        if self.started:
            # Cancel "start" courouting after the session has been created
            self.cancel_pending_task("shell_manager")
            return
        if ShellSession.started == True:
            # Wait until we've received the start command to start creating a session
            self.started = True
            self.start()
            logger.info("SHELL STARTED")

    def start(self):

        endpoints = None
        if not self.destination:
            try:
                endpoints = utils.get_endpoint_rms(f"slave {ShellSession.nodeID}")

            except:
                logger.info("Unable to find endpoints")
            msg = {'uid': self.uid}
            if endpoints and endpoints["peer_ids"]:
                peer_ids = endpoints["peer_ids"]
                peer_info = peer_ids[0]
                self.destination = peer_info

        async def start_communication():
            if self.once:
                return
            while ShellSession.command == None:
                # Waiting to receive command
                await asyncio.sleep(2)

            ShellSession.lock = 1
            command = ShellSession.command
            ShellSession.command = None
            ShellSession.lock = 0
            self.lastinput = command
            # if command == 'exit':
            #     ShellSession.lock = 1
            #     ShellSession.response = "Session closed"
            #     ShellSession.responded = True
            #     ShellSession.started = False
            #     self.started = False
            #     ShellSession.lock = 0
            #     self.cancel_pending_task(name="start_communication")
            #     self.register_task("shell_manager",
            #                self.shell_manager, interval=0.5, delay=0)
                # asyncio.get_event_loop().stop()
                # return
            msg['command'] = self.encrypt_message(content=str.encode(command), peer_info=self.destination)
            msg['master'] = True
            await self.send(self.destination, msg)
            self.received_response = False
            self.received_ack = False
            counter = 0
            while not self.received_ack:
                if counter >= 1500:
                    #Find a new relay, create a session and send the message
                    self.recreate_session(self.destination, msg)
                counter +=1
                await asyncio.sleep(1/100)

        self.register_task("start_communication",
                           start_communication, interval=0.1, delay=0)

    async def query(self):

        reg = {
            'peer_id': '',
            'ip_addrs': '',
            'services': [],
        }

        ip_peer = None

        service_desc = {}
        service_meta = {}
        peer_info = {}
        addrs = []
        service_meta['name'] = "master"

        service_desc['name'] = "name"
        service_desc['endpoints'] = "addrs"
        service_meta['service_input'] = ''
        service_meta['service_output'] = ''
        service_meta['price'] = 0
        peer_info['nodeID'] = self.uid
        peer_info['key'] = self.my_peer.public_key.key_to_bin()
        peer_info['mid'] = self.my_peer.mid
        peer_info['public_key'] = self.my_peer.public_key.key_to_bin()
        peer_info['_address'] = self.my_peer._address
        ip_peer = self.my_peer._address
        peer_info['_address'] = ip_peer
        reg['peer_id'] = peer_info
        reg['ip_addrs'] = ["node_ip", "adapter_port"]
        reg['services'].append(service_meta)

        # Updates global orchestrator for automatic service deployment
        channel = grpc.insecure_channel(str(registry_address_rms))
        stub = service_pb2_grpc.RegistryStub(channel)
        services = pickle.dumps(reg)
        try:
            if ip_peer != None and "UDPv4Address(ip='192" not in str(ip_peer) and "UDPv4Address(ip='127" not in str(ip_peer) and "UDPv4Address(ip='172" not in str(ip_peer) and "UDPv4Address(ip='10." not in str(ip_peer):
                response = stub.updateRegistry(
                    service_pb2.Services(services_info=services))
        except Exception as e:
            print(e)
