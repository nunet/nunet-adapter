import subprocess
import json
import threading
import requests
import sys
import traceback
import logging
import os
from datetime import datetime
import time
from config import config
import ast

import grpc

sys.path.append("./service_spec")


import stats_db_pb2
import stats_db_pb2_grpc

stats_db_address = os.environ['stats_db_address']


class snet_utils():
    def __init__(self):
        self.private_key=config["private_key"]

    def create_account(self):
        subprocess.check_output(["bash","script.sh","create_account","snet",str(self.private_key)])

    def agi_transfer(self,public_key,amount,call_id,service_name):
        value=""
        while "transactionHash" not in str(value):
            try:
                value=subprocess.check_output(["bash","script.sh","transfer","snet",str(public_key),str(amount)])
            except Exception as e:
                logging.exception("message",e)
                time.sleep(1)
        value=value.decode()
        channel_db=grpc.insecure_channel("{}".format(str(stats_db_address)))
        stub = stats_db_pb2_grpc.StatsDatabaseStub(channel_db)
        txn_hash=value[-69:-3]
        stub.db_txn_agi(stats_db_pb2.DatabaseAgiInput(txn_hash=txn_hash,call_id=call_id,service_name=service_name,amount=str(amount)))
    
    def create_channel(self,name):
        subprocess.check_output(["bash","script.sh","create_channel",str(name)])

    def account_setup(self,name):
        #switch account to user identity
        try:
            value=subprocess.check_output( ["bash","script.sh","account_switch",str(name)])
        except Exception as e:
            logging.exception("message",e)
            return "account doesn't exist"
        #switch network
        value=subprocess.check_output( ["bash","script.sh","network_switch"])
        #get channel
        value=subprocess.check_output( ["bash","script.sh","get_channel"])
        channel_id=str(value).split(" ")[8]
        return channel_id
    
    def account_deposit(self):
        self.count=1
        try:
            value=subprocess.check_output( ["bash", "script.sh","deposit"])
        except Exception as e:
            logging.exception("message",e)
        finally:
            self.count=0
    

    def handle_channel(self,name):
        try:
            return self.account_setup(name)
        except:
            try:
                self.create_channel(name)
                return self.account_setup(name)
            except Exception as e:
                logging.exception("message:"+str(e))
                return "no_balance"

    def channel_block(self):
        channel_block=subprocess.check_output(["snet", "channel", "print-all-filter-sender"])
        channel_block=int(str(channel_block).split("\\n")[-2].split(" ")[-1])
        return channel_block

    def unspent_amount(self,channel_id):
        f=open("constants.json")
        daemon_endpoint=json.load(f)['daemon_endpoint']
        unspent_amount=subprocess.check_output(["snet","client","get-channel-state",
                                                str(channel_id),str(daemon_endpoint)])
        unspent_amount=int(str(unspent_amount).split("\\n'")[-2].split(" ")[-1])
        return unspent_amount


