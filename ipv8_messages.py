from ipv8.messaging.lazy_payload import VariablePayload, vp_compile
from ipv8.lazy_community import lazy_wrapper

@vp_compile
class CallAdapterMsg(VariablePayload):
    msg_id = 1
    format_list = ['raw']
    names = ["parameters"]

@vp_compile
class CallAdapterResult(VariablePayload):
    msg_id = 2
    format_list = ['raw']
    names = ['result']

@vp_compile
class reqAdapterMetadataMsg(VariablePayload):
    msg_id = 3
    format_list = ['raw']
    names = ['parameters']

@vp_compile
class reqAdapterMetadataResult(VariablePayload):
    msg_id = 4
    format_list = ['raw']
    names = ['result']

@vp_compile
class reqServiceMsg(VariablePayload):
    msg_id = 5
    format_list = ['raw']
    names = ["parameters"]

@vp_compile
class reqServiceResult(VariablePayload):
    msg_id = 6
    format_list = ['raw']
    names = ['result']

@vp_compile
class ReqPriceMsg(VariablePayload):
    msg_id = 7
    format_list = ['raw']
    names = ["parameters"]

@vp_compile
class ReqPriceMsgResult(VariablePayload):
    msg_id = 8
    format_list = ['raw']
    names = ['result']

@vp_compile
class reqServiceResultRestApi(VariablePayload):
    msg_id = 9
    format_list = ['raw']
    names = ['result']

@vp_compile
class HealthCheckSender(VariablePayload):
    msg_id = 10
    format_list = ['raw']
    names = ['result']

@vp_compile
class HealthCheckReceiver(VariablePayload):
    msg_id = 11
    format_list = ['raw']
    names = ['result']

@vp_compile
class PeerInfo(VariablePayload):
    msg_id = 12
    format_list = ['raw']
    names = ["parameters"]

@vp_compile
class SendMessage(VariablePayload):
    msg_id = 13
    format_list = ['raw']
    names = ["parameters"]

@vp_compile
class ReceiveMessage(VariablePayload):
    msg_id = 14
    format_list = ['raw']
    names = ['result']