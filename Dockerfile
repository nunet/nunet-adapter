FROM ubuntu:18.04

RUN apt-get update && apt-get install --no-install-recommends software-properties-common -y && rm -rf /var/lib/apt/lists/*

RUN add-apt-repository ppa:deadsnakes/ppa
RUN apt-get update && apt-get install --no-install-recommends  -y python3.7-venv python3-pip &&  rm -rf /var/lib/apt/lists/*
RUN update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.7 1
RUN apt-get update && apt-get install --no-install-recommends -y libsodium-dev \
    python3-dev \
    gcc \
    curl \ 
    libffi-dev \
    libssl-dev \
    libusb-1.0-0.dev \
    libudev1 \
    libudev-dev \
    libjpeg-dev \
    zlib1g-dev \
    libpq-dev \
    libgmp-dev \
    scons \
    swig && apt install libpython3.7-dev -y && rm -rf /var/lib/apt/lists/*

# RUN apt install libpython3.7-dev -y

RUN pip3 install --upgrade pip
RUN pip3 install --upgrade setuptools
RUN python3.7 -m venv /venv
ENV PATH=/venv/bin:$PATH

COPY requirements.txt /

RUN pip3 install --ignore-installed PyYAML
RUN pip3 install --upgrade pip
RUN pip3 install --upgrade setuptools
RUN pip3 install -r /requirements.txt
# RUN apt-get update 

RUN pip3 install snet-cli==2.0.4

RUN mkdir p /root/.snet && echo  \
    "[network.ropsten] \n\
    default_eth_rpc_endpoint = https://ropsten.infura.io/v3/a6cb61711f8e48779c3cd94cbb2b8721 \n\
    default_gas_price = medium \n\
    \n\
    [ipfs]\n\
    default_ipfs_endpoint = http://ipfs.singularitynet.io:80 \n\
    \n\
    [session]\n\
    network = ropsten" > /root/.snet/config


EXPOSE 9998


ARG tokenomics_api_address



ENV tokenomics_api_address=${tokenomics_api_address}

ENV registry_address_prod=195.201.197.25:4558

ENV registry_address_test=195.201.197.25:4658

ENV stats_db_address=195.201.197.25:30003

ENV jaeger_address=testserver.nunet.io



# RUN pip3 install opentelemetry-launcher
# RUN opentelemetry-bootstrap -a install



RUN pip3 install opentelemetry-exporter-jaeger
RUN pip3 install opentelemetry-api
RUN pip3 install opentelemetry-sdk
RUN pip3 install opentelemetry-instrumentation-grpc

ENV LS_SERVICE_NAME=nunet-adapter
ENV LS_ACCESS_TOKEN=ej4S2fSomE5V7uP/LYJLw9frBRnBRqHVoQiRYSCwoSld1MncIAHByOSkc8jcKiSAbhVEy2zI0Emjw38vVF8c87vFd6Q1wMCnI/DD4/Bi
ENV OTEL_PYTHON_TRACER_PROVIDER=sdk_tracer_provider
ENV jaeger_address=testserver.nunet.io

ENV tokenomics_address=135.181.222.170

ENV tokenomics_port_test=4557

ENV tokenomics_port_prod=4556

COPY . /NUNET-ADAPTER 
WORKDIR /NUNET-ADAPTER


RUN sh buildproto.sh

CMD ["python3", "nunet_adapter.py"]
