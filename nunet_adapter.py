#!/usr/bin/env python3

from concurrent import futures
import sys
import grpc
import time
import asyncio
import ast
import threading
import uuid

from ipv8.community import Community
from ipv8.configuration import ConfigBuilder, Strategy, WalkerDefinition, default_bootstrap_defs
from ipv8_service import IPv8
from ipv8.lazy_community import lazy_wrapper
from session import Session
from asyncio import ensure_future, get_event_loop
import ipv8_messages
import pickle

from ipv8.peer import Peer
from ipv8.keyvault.public.libnaclkey import LibNaCLPK
from session import Session
from sharedRegistry import SharedRegistry
from shellsession import ShellSession

sys.path.append("./service_spec")
import nunet_adapter_pb2
import nunet_adapter_pb2_grpc

import service_proto_pb2
import service_proto_pb2_grpc

import tokenomics_pb2
import tokenomics_pb2_grpc

import service_pb2
import service_pb2_grpc

import log
import time
import subprocess

import os
import google.protobuf.json_format

import utils

import json

from snet_utils import *

import stats_db_pb2
import stats_db_pb2_grpc

from time import sleep
import datetime

import opentelemetry
from opentelemetry import trace
from opentelemetry.instrumentation.grpc import GrpcInstrumentorServer
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.exporter.jaeger.thrift import JaegerExporter
from opentelemetry.sdk.trace.export import BatchSpanProcessor
from opentelemetry.trace import NonRecordingSpan, SpanContext, TraceFlags
from opentelemetry.trace import set_span_in_context

import threading

import binascii

class HexBytesEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, bytes):
            return binascii.hexlify(obj).decode('utf-8')
        return json.JSONEncoder.default(self,obj)


jaeger_address = os.environ['jaeger_address']


jaeger_exporter = JaegerExporter(
    agent_host_name=jaeger_address,
    agent_port=6831,
)


trace_provider=trace.get_tracer_provider().add_span_processor(
    BatchSpanProcessor(jaeger_exporter)
)

grpc_server_instrumentor = GrpcInstrumentorServer()
grpc_server_instrumentor.instrument()

tracer_server=opentelemetry.instrumentation.grpc.server_interceptor(tracer_provider=trace_provider)
tracer_grpc=trace.get_tracer(tracer_server)

tracer = trace.get_tracer(__name__)



snet_utils = snet_utils()
snet_utils.create_account()


tokenomics_api_name = os.environ['tokenomics_api_name']

deployment_type = os.environ['deployment_type']


logger = log.setup_custom_logger('TRACE')




if len(sys.argv) == 2:
    grpc_port = sys.argv[1]
else:
    grpc_port = "9998"


registry_address_prod = os.environ['registry_address_prod']
registry_address_test = os.environ['registry_address_test']

stats_db_address = os.environ['stats_db_address']


if deployment_type=="prod":
    registry_address=registry_address_prod
else:
    registry_address=registry_address_test

device_name=os.environ['LS_SERVICE_NAME']

class NunetAdapter(nunet_adapter_pb2_grpc.NunetAdapterServicer,Community,SharedRegistry, ShellSession):

    community_id = b'\xc2\x98da\x0b\xa7LN\x8a(\xc1\xfcV\xf7m0\xc5s\xc2\x99'
    

    def __init__(self,my_peer,endpoint,network,max_peers=100, *args, **kwargs):
        super().__init__(my_peer,endpoint,network, *args, **kwargs)
        self.max_peers = max_peers
        self._lock = threading.RLock()
        self.call_adapter_result = {}
        self.health_check_result = {}
        self.req_adapter_metadata_result = {}
        self.req_price_result = {}
        self.tracer_infos = {}
        self.checked_peer_infos = {}
        self.agi_queue = []
        self.message_queue = []

        self.add_message_handler(ipv8_messages.CallAdapterMsg,self.call_adapter_handler)
        self.add_message_handler(ipv8_messages.CallAdapterResult,self.call_adapter_result_handler)
        self.add_message_handler(ipv8_messages.reqAdapterMetadataMsg,self.reqAdapterMetadata_handler)
        self.add_message_handler(ipv8_messages.reqAdapterMetadataResult,self.reqAdapterMetadata_result_handler)
        self.add_message_handler(ipv8_messages.reqServiceMsg,self.reqService_handler)
        self.add_message_handler(ipv8_messages.ReqPriceMsg,self.req_price_handler)
        self.add_message_handler(ipv8_messages.ReqPriceMsgResult,self.req_price_result_handler)
        self.add_message_handler(ipv8_messages.HealthCheckSender,self.health_check_sender_result_handler)
        self.add_message_handler(ipv8_messages.HealthCheckReceiver,self.health_check_receiver_result_handler)

        self.add_message_handler(ipv8_messages.SendMessage,self.send_message_handler)
        self.add_message_handler(ipv8_messages.ReceiveMessage,self.receive_message_handler)

    @lazy_wrapper(ipv8_messages.CallAdapterMsg)
    async def call_adapter_handler(self,peer,payload):
        logger.info("Inside call_adapter_handler")
        msg = utils.decode_message(payload.parameters)
        uid = msg["uid"]
        tracer_info = msg["tracer_info"]
        trace_id = tracer_info["trace_id"]
        span_id = tracer_info["span_id"]
        logger.info(msg)

        span_context = SpanContext(
        trace_id=trace_id,
        span_id=span_id,
        is_remote=True,
        trace_flags=TraceFlags(0x01)
        )
        ctx = trace.set_span_in_context(NonRecordingSpan(span_context))


        with tracer.start_as_current_span("callAdapter_handler", context = ctx) as span:

            trace_info=span.get_span_context()
            span_id = trace_info.span_id
            trace_id = trace_info.trace_id

            sleep(30 / 1000)

            logger.info("Inside call_adapter_handler")

            fp = nunet_adapter_pb2.ServiceDefnitionWorkflow()
            fp.service_name = msg["service_name"]
            fp.params = msg["params"]
            fp.service_input_params=msg["service_input_params"]
            fp.declarations = msg["declarations"]
            fp.tracer_info = str({
                'trace_id': trace_id,
                'span_id': span_id
            })
            

            address = "localhost" + ":" + str(grpc_port)
            async with grpc.aio.insecure_channel("{}".format(address)) as channel:
                stub = nunet_adapter_pb2_grpc.NunetAdapterStub(channel)
                logger.info("Calling reqServiceWorkflow")
                result = await stub.reqServiceWorkflow(fp)
                logger.info(f"result == {result}")
                return_result = {}
                return_result["result"] = result
                return_result["uid"] = uid
            span.add_event("event message", {"reqServiceWorkflow result": str(return_result)})
            sleep(30/1000)
            encoded_result = utils.encode_message(return_result)
            self.ez_send(peer,ipv8_messages.CallAdapterResult(encoded_result))
            logger.info("Result sent")

    
    @lazy_wrapper(ipv8_messages.reqAdapterMetadataMsg)
    async def reqAdapterMetadata_handler(self,peer,payload):
        with tracer.start_as_current_span("reqAdapterMetadata_handler",) as span:
            logger.info("Inside reqAdapterMetadata_handler")
            msg = utils.decode_message(payload.parameters)
            fp = nunet_adapter_pb2.metaServiceParams()
            fp.service_name = msg["service_name"]
            uid = msg["uid"]
            address = "localhost" + ":" + str(grpc_port)
            async with grpc.aio.insecure_channel("{}".format(address)) as channel:
                stub = nunet_adapter_pb2_grpc.NunetAdapterStub(channel)
                return_result = {}
                result = await stub.reqServiceMetadata(fp)
                return_result["result"] = result
                return_result["uid"] = uid
            span.add_event("event message", {"service metadata": str(return_result)})
            sleep(30/1000)
            encoded_result = utils.encode_message(return_result)
            self.ez_send(peer,ipv8_messages.reqAdapterMetadataResult(encoded_result))

    @lazy_wrapper(ipv8_messages.reqServiceMsg)
    async def reqService_handler(self,peer,payload):
        msg = utils.decode_message(payload.parameters)
        trace_id = msg['trace_id']
        span_id = msg['span_id']
        result = msg["result"]
        call_id = msg["call_id"]
        

        logger.info(trace_id)
        logger.info(span_id)
        span_context = SpanContext(
        trace_id=trace_id,
        span_id=span_id,
        is_remote=True,
        trace_flags=TraceFlags(0x01)
        )

        ctx = trace.set_span_in_context(NonRecordingSpan(span_context))
        with tracer.start_as_current_span("reqService_handler", context = ctx) as span:
            logger.info("Inside reqService_handler")
            trace_info=span.get_span_context()

            span_id = trace_info.span_id
            trace_id = trace_info.trace_id

            self.tracer_infos[call_id] = {
                'trace_id': trace_id,
                'span_id': span_id
            }
            result = msg["result"]
            call_id = msg["call_id"]
            fp = nunet_adapter_pb2.ServiceDefnition()
            fp.service_name = msg["service_name"]
            fp.params = '{"body":"'+msg["body"]+'","headline":"'+msg["headline"]+'","call_id":"'+str(msg["call_id"])+'"}'
            address = "localhost" + ":" + str(grpc_port)
            async with grpc.aio.insecure_channel("{}".format(address)) as channel:
                stub = nunet_adapter_pb2_grpc.NunetAdapterStub(channel)
                response = await stub.reqService(fp)
                logger.info(response)
                # response = next(response)
                if result["return_code"] == 0:
                    return_result= {
                    "response":response.service_response,
                    "return_code":result["return_code"],
                    "call_id":str(call_id)
                    }
                    span.add_event("event message", {"reqService result": str(return_result)})
                    sleep(30/1000)
                else:
                    return_result =  {"response":None,"return_code":result["return_code"]}
                    span.add_event("event message", {"reqService result": str(return_result)})
                    sleep(30/1000)
            encoded_result = utils.encode_message(return_result)
            self.ez_send(peer,ipv8_messages.reqServiceResultRestApi(encoded_result))

    @lazy_wrapper(ipv8_messages.ReqPriceMsg)
    async def req_price_handler(self,peer,payload):
        logger.info("Inside req_price_handler")
        msg = utils.decode_message(payload.parameters)
        #logger.info(msg)

        uid = msg["uid"]
        service_name = msg["service_name"]

        tracer_info = msg["tracer_info"]
        trace_id = tracer_info['trace_id']
        span_id = tracer_info['span_id']

        span_context = SpanContext(
        trace_id=trace_id,
        span_id=span_id,
        is_remote=True,
        trace_flags=TraceFlags(0x01)
        )
        ctx = trace.set_span_in_context(NonRecordingSpan(span_context))
        with tracer.start_as_current_span("req_price_handler",context = ctx) as span:
            trace_info=span.get_span_context()
            span_id = trace_info.span_id
            trace_id = trace_info.trace_id

            address = "localhost" + ":" + str(grpc_port)

            async with grpc.aio.insecure_channel("{}".format(address)) as channel:
                stub = nunet_adapter_pb2_grpc.NunetAdapterStub(channel)
                fp = nunet_adapter_pb2.priceParams()
                fp.service_name = service_name
                fp.tracer_info = str({
                    'trace_id': trace_id,
                    'span_id': span_id
                })
                result = await stub.reqPrice(fp)
                cost_per_process = result.cost_per_process
                pubk = result.pubk
                logger.info(cost_per_process)
                logger.info(pubk)

            return_result = {}
            return_result["public_key"] = pubk
            return_result["cost_per_process"] = cost_per_process
            return_result["uid"] = uid
            span.add_event("event message", {"reqPrice result": str(return_result)})
            sleep(30/1000)
            encoded_result = utils.encode_message(return_result)

            
            self.ez_send(peer,ipv8_messages.ReqPriceMsgResult(encoded_result))
            logger.info("Replied back reqPrice_handler")
  
    @lazy_wrapper(ipv8_messages.CallAdapterResult)
    def call_adapter_result_handler(self,peer,payload):
        msg = utils.decode_message(payload.result)
        uid = msg["uid"]
        result = msg["result"]
        logger.info(f"Received result from peer: {peer}, uid == {uid}, result == {result}")
        with self._lock:
            logger.info("Inside lock")
            self.call_adapter_result[uid] = result
        logger.info("Added returned address into dict")

    @lazy_wrapper(ipv8_messages.reqAdapterMetadataResult)
    def reqAdapterMetadata_result_handler(self,peer,payload):
        msg = utils.decode_message(payload.result)
        result = msg["result"]
        uid = msg["uid"]
        logger.info(f"Received result from peer: {peer}")
        with self._lock:
            self.req_adapter_metadata_result[uid] = result

    @lazy_wrapper(ipv8_messages.ReqPriceMsgResult)
    def req_price_result_handler(self,peer,payload):
        msg = utils.decode_message(payload.result)
        uid = msg["uid"]
        public_key = msg["public_key"]
        cost_per_process = msg["cost_per_process"]
        self.req_price_result[uid] = msg
        logger.info("Added reqPrice result to dict")


    @lazy_wrapper(ipv8_messages.ReceiveMessage)
    def receive_message_handler(self,peer,payload):
        msg = utils.decode_message(payload.result)
        uid = msg["uid"]
        sender = msg["sender"]
        msg_time = msg["msg_time"]
        logger.info(msg)
        logger.info("message confirmed")

    @lazy_wrapper(ipv8_messages.SendMessage)
    def send_message_handler(self,peer,payload):
        msg = utils.decode_message(payload.parameters)
        uid = msg["uid"]
        logger.info(msg)
        self.message_queue.append(msg)
        msg = {}
        msg["message"] = "message recieved"
        msg["uid"] = "uid"
        msg["sender"] = "sender"
        msg["msg_time"] = "msg_time"
        encoded_msg = utils.encode_message(msg)
        self.ez_send(peer,ipv8_messages.ReceiveMessage(encoded_msg))

    @lazy_wrapper(ipv8_messages.HealthCheckSender)
    def health_check_sender_result_handler(self,peer,payload):
        logger.info("Health check passed")
        msg = utils.decode_message(payload.result)
        uid = msg["uid"]
        self.health_check_result[uid] = msg

    @lazy_wrapper(ipv8_messages.HealthCheckReceiver)
    def health_check_receiver_result_handler(self,peer,payload):
        logger.info("Health check reveived")
        msg = utils.decode_message(payload.result)
        uid = msg["uid"]
        service_name = msg["service_name"]
        req_address = utils.get_service_address(SharedRegistry.service_map,service_name)
        address = req_address[0]
        port = req_address[1]
        logger.info(address)
        logger.info(port)
        metaParams = service_proto_pb2.metaParams()
        metaParams.service_name = service_name
        metaParams.tracer_info = str({
            'trace_id': 1,
            'span_id': 1
        })
        serviceAddress = address+":"+port
        temp=0
        cnt=0
        while temp==0 and cnt<10:
            try:
                channel = grpc.insecure_channel("{}".format(serviceAddress))
                stub = service_proto_pb2_grpc.ProtoDefnitionStub(channel)
                temp=1
            except Exception as e:
                try:
                    req_address = utils.get_service_address(SharedRegistry.service_map,service_name)
                    address = req_address[0]
                    port = req_address[1]
                    logger.info(address)
                    logger.info(port)
                    serviceAddress = address+":"+port
                except Exception as e:
                    logger.error(e)
                
            cnt+=1
        res = stub.req_metadata(metaParams)
        
        encoded_result = utils.encode_message(msg)
        self.ez_send(peer,ipv8_messages.HealthCheckSender(encoded_result))


    async def reqService(self, req, ctxt):
        current_span = trace.get_current_span()
        current_span.set_attribute("http.route", "some_route")
        
        sleep(30 / 1000)
        # req = next(request_iterator)

        call_id = eval(req.params)['call_id']

        #for grpc fall back if trance info not set by ipv8 handler
        try:
            ids = self.tracer_infos[call_id]
        except Exception as e:
            logger.error(e)
            #trace_info=current_span.get_span_context()
            #span_id = trace_info.span_id
            #trace_id = trace_info.trace_id
            span_id = eval(req.params)['span_id']
            trace_id = eval(req.params)['trace_id']

            self.tracer_infos[call_id] = {
                'trace_id': eval(trace_id),
                'span_id': int(span_id)
            }
            ids = self.tracer_infos[call_id]
        
        span_context = SpanContext(
        trace_id=ids['trace_id'],
        span_id=ids['span_id'],
        is_remote=True,
        trace_flags=TraceFlags(0x01)
        )

        ctx = trace.set_span_in_context(NonRecordingSpan(span_context))
        with tracer.start_as_current_span("reqService",context = ctx) as span:

            trace_info=span.get_span_context()
            span_id = trace_info.span_id
            trace_id = trace_info.trace_id

            call_id = eval(req.params)['call_id']

            service_name = req.service_name
            params = eval(req.params)
            params['tracer_info'] = str({
                'trace_id': trace_id,
                'span_id': span_id
            })
            params = str(params)
        
            try:
                req_address = utils.get_service_address(SharedRegistry.service_map,service_name)
                address = req_address[0]
                port = req_address[1]
                logger.info(address)
                logger.info(port)
            except Exception as e:
                exception = Exception(str(e))
                span.record_exception(exception)
                span.set_status(Status(StatusCode.ERROR, "error happened"))
                logger.error(e)
            serviceAddress = address+":"+port
            temp=0
            cnt=0

            # channel = grpc.aio.insecure_channel("{}".format(serviceAddress))
            async with grpc.aio.insecure_channel("{}".format(serviceAddress)) as channel:
                stub = service_proto_pb2_grpc.ProtoDefnitionStub(channel)
                temp=1

                protoParams = service_proto_pb2.protoParams()
                res = await stub.req_msg(protoParams)
            proto_file_name = "proto_"+service_name+".proto"
            f = open(proto_file_name, "w")
            f.write(res.proto_defnition)
            f.close()
            service_input_params=res.service_input_params
            service_input_params=eval(service_input_params)
            inputs=""
            logger.info(str(service_input_params))
            i=0
            p=ast.literal_eval(params)
            for param in service_input_params:
                logger.info(str(param))
                logger.info(p[param])
                if param in list(ast.literal_eval(params).keys()):
                    value=p[param]
                    value=value.replace("\'","\\\'").replace("\"","\\\"")
                    value=value.replace("\n"," ").replace("\t"," ")
                    value=value.replace("\b"," ").replace("\r"," ")
                    value=value.replace("\f"," ")

                    if i==0:
                        logger.info(str(i))
                        inputs+=param+"="+'"'+value+'"'
                    else:
                        inputs+=", "+param+"="+'"'+value+'"'
                    i=1
            logger.info(str(inputs))
            subprocess.call(["python3", "-m", "grpc_tools.protoc",
                            "-I.", "--python_out=.", "--grpc_python_out=.",
                            proto_file_name])

            service_name = service_name.replace("-", "_")
            proto_service_pb2 = "proto_"+service_name+"_pb2"
            proto_service_pb2_grpc = "proto_"+service_name+"_pb2_grpc"
            proto_pb2 = __import__(proto_service_pb2)
            proto_pb2_grpc = __import__(proto_service_pb2_grpc)

            async with grpc.aio.insecure_channel("{}".format(serviceAddress)) as channel:
                stub = getattr(proto_pb2_grpc, res.service_stub)(channel)
                param_string="getattr(proto_pb2, res.service_input)("+inputs+")"
                logger.info(param_string)
                inp=eval(param_string)
                logger.info(inp)
                # with getattr(stub, res.function_name) as func:
                #     result = await func(inp)
                result = await (getattr(stub, res.function_name)(inp))
                result = google.protobuf.json_format.MessageToJson(result)
            logger.info("Returned result from reqService")
            return nunet_adapter_pb2.ServiceResponse(service_response=result)

    async def telemetry(self, request, context):
        stance_pred = request.result
        cpu_used = request.cpu_used
        memory_used = request.memory_used
        time_taken = request.time_taken
        net_used = request.net_used
        device_name = request.device_name
        call_id = request.call_id
        service_name = request.service_name
        tracer_info = eval(request.tracer_info)

        trace_id = tracer_info['trace_id']
        span_id = tracer_info['span_id']

        span_context = SpanContext(
        trace_id=trace_id,
        span_id=span_id,
        is_remote=True,
        trace_flags=TraceFlags(0x01)
        )
        ctx = trace.set_span_in_context(NonRecordingSpan(span_context))
        with tracer.start_as_current_span("telemetry", context=ctx) as span:
            stat = {
                "time_taken": time_taken,
                "total_memory": memory_used,
                "net_rx": net_used,
                "cpu_usage": cpu_used
            }
            logger.info(stat)
            
            try:    
                channel_db=grpc.insecure_channel("{}".format(str(stats_db_address)))
                stub = stats_db_pb2_grpc.StatsDatabaseStub(channel_db)
                result=stub.db_add(stats_db_pb2.DatabaseTelemetryInput(cpu_used=cpu_used,memory_used=memory_used,net_used=net_used,time_taken=time_taken,device_name=device_name))
                logger.info(result)
            except Exception as e:
                logger.error(e)
            try:
                req_address = utils.get_address(tokenomics_api_name)
                ip_addr = req_address[1]
                address = ip_addr[0]
                port = ip_addr[1]
                logger.info(address)
                logger.info(port)
            except Exception as e:
                logger.error(e)
            channel = grpc.insecure_channel("{}".format(address+":"+port))
            stub = tokenomics_pb2_grpc.TokenomicsStub(channel)
            result = [stance_pred, stat]
            logging.info(result)
            result = str(result)
            fp=tokenomics_pb2.SaveResultInput()
            fp.result=result
            fp.call_id=call_id
            fp.service_name=service_name
            save_result=threading.Thread(target=stub.saveResult, args=(fp,))
            save_result.start()
            logger.info("thread returned")
            return nunet_adapter_pb2.TelemetryOutput(response=result)

    async def callAdapter(self, req, ctxt):
        current_span = trace.get_current_span()
        current_span.set_attribute("http.route", "some_route")
        
        sleep(30 / 1000)

        tracer_info = req.tracer_info
        tracer_info = eval(tracer_info)

        span_context = SpanContext(
        trace_id=tracer_info['trace_id'],
        span_id=tracer_info['span_id'],
        is_remote=True,
        trace_flags=TraceFlags(0x01)
        )

        ctx = trace.set_span_in_context(NonRecordingSpan(span_context))
        with tracer.start_as_current_span("callAdapter",context = ctx) as span:
            trace_info=span.get_span_context()
            span_id = trace_info.span_id
            trace_id = trace_info.trace_id
            
            uid = uuid.uuid1()
            self.call_adapter_result[uid] = None
            try:
                # req = next(request_iterator)
                logger.info("Received request from serivce")
            except Exception as e:
                logger.error(e)

            service_name = req.service_name
            declarations = req.declarations
            params = req.params
            service_input_params = req.service_input_params
            
            service_id=str(service_name)+str(trace_id)
            logger.info(service_id)

            self.health_check_result[uid]=None
            msg = {}
            msg["service_name"] = service_name
            msg["params"] = params
            msg["declarations"] = declarations
            msg["service_input_params"] = service_input_params
            msg["uid"] = uid
            encoded_msg = utils.encode_message(msg)
            try:
                peer_public_key=self.checked_peer_infos[service_id]
                logger.info(peer_public_key)
                endpoints = SharedRegistry.getServiceEndpoints(service_name)
                if not endpoints:
                    logger.info("No endpoints found for service {} in internal registry ".format(service_name))
                    endpoints = utils.get_endpoints(service_name)
    
                peer_ids = endpoints["peer_infos"]
                ip_addrs = endpoints["endpoints"]
                for peer_info in peer_ids:
                    key = LibNaCLPK(peer_info['key'])
                    peer = Peer(key)
                    peer.mid = peer_info['mid']
                    peer.public_key = peer_info['public_key']
                    peer._address = peer_info['_address']
                    logger.info(peer_info["public_key"])
                    if peer_public_key == peer_info["public_key"]:
                        logger.info(peer_info["_address"])
                        key = LibNaCLPK(peer_info['key'])
                        peer_2 = Peer(key)
                        peer_2.mid = peer_info['mid']
                        peer_2.public_key = peer_info['public_key']
                        peer_2._address = peer_info['_address']
                peer=peer_2

            except Exception as e:
                exception = Exception(str(e))
                span.record_exception(exception)
                span.set_status(Status(StatusCode.ERROR, "error happened"))
                logger.error(e)
            
            span.add_event("event message", {"service name": service_name})
            sleep(30 / 1000)
            span.add_event("event message", {"peer address": str(peer)})
            sleep(30 / 1000)

            msg = {}
            msg["service_name"] = service_name
            msg["params"] = params
            msg["declarations"] = declarations
            msg["service_input_params"] = service_input_params
            msg["uid"] = uid
            msg["tracer_info"] = {
                'trace_id': trace_id,
                'span_id': span_id
            }
            encoded_msg = utils.encode_message(msg)
            self.ez_send(peer,ipv8_messages.CallAdapterMsg(encoded_msg))
            logger.info("IPv8 message sent")


            start_time = time.time()
            timeout = 50
            ipv8 = True

            while self.call_adapter_result[uid] == None:
                if time.time() > start_time + timeout:
                    ipv8 = False
                    break
                await asyncio.sleep(1/1000)
            
            if not ipv8:
                logger.info("IPv8 connection timed out trying gRPC")
                adapterAddress = address+":"+port
                async with grpc.aio.insecure_channel("{}".format(adapterAddress)) as channel:
                    span.add_event("event message", {"adapter address": str(adapterAddress)})
                    sleep(30 / 1000)
                    stub = nunet_adapter_pb2_grpc.NunetAdapterStub(channel)
                    fp = nunet_adapter_pb2.ServiceDefnitionWorkflow()
                    fp.service_name = service_name
                    fp.params =  params
                    fp.service_input_params=service_input_params
                    fp.declarations = declarations
                    fp.tracer_info = str({
                        'trace_id': trace_id,
                        'span_id': span_id
                    })
                    result = await stub.reqServiceWorkflow(fp)
                    span.add_event("event message", {"result": str(result)})
                    sleep(30 / 1000)
                return result

            if ipv8:
                logger.info("Returning result from IPv8")
                span.add_event("event message", {"result": str(self.call_adapter_result[uid])})
                
                sleep(30 / 1000)
                
                return self.call_adapter_result[uid]

    async def signAdapterTransaction(self, req, ctxt):
        current_span = trace.get_current_span()
        current_span.set_attribute("http.route", "some_route")
        
        sleep(30 / 1000)
        cost_per_process = req.cost_per_process
        pubk = req.pubk
        call_id = req.call_id
        
        current_span.add_event("event message", {"public key": str(pubk)})
        current_span.add_event("event message", {"cost_per_process": str(cost_per_process)})

        try:
            req_address = utils.get_address(tokenomics_api_name)
            ip_addr = req_address[1]
            address = ip_addr[0]
            port = ip_addr[1]
            logger.info(address)
            logger.info(port)
        except Exception as e:
            exception = Exception(str(e))
            span.record_exception(exception)
            span.set_status(Status(StatusCode.ERROR, "error happened"))
            logger.error(e)
        
        current_span.add_event("event message", {"tokenomics api address": str(address)+":"+str(port)})
        channel = grpc.insecure_channel("{}".format(address+":"+port))
        stub_tokenomics = tokenomics_pb2_grpc.TokenomicsStub(channel)
        fp=tokenomics_pb2.SignTransactionInput()
        fp.cost_per_process=int(float(cost_per_process))
        fp.pubk=pubk
        fp.call_id=call_id
        create_escrow= threading.Thread(target=stub_tokenomics.signTransaction, args=(fp,))
        create_escrow.start()
        # new_loop = asyncio.new_event_loop()
        # # executor = futures.ThreadPoolExecutor(max_workers=1)
        # logger.info("***** Going to run signTransaction in background ****")
        # await new_loop.run_in_executor(None,stub_tokenomics.signTransaction,fp)
        # logger.info("***** Running signTransaction in background ****")
        return call_id

    async def getAdapterResult(self, req, ctxt):
        current_span = trace.get_current_span()
        current_span.set_attribute("http.route", "some_route")
        
        sleep(30 / 1000)
        try:
            req_address = utils.get_address(tokenomics_api_name)
            ip_addr = req_address[1]
            address = ip_addr[0]
            port = ip_addr[1]
            logger.info(address)
            logger.info(port)
        except Exception as e:
            exception = Exception(str(e))
            span.record_exception(exception)
            span.set_status(Status(StatusCode.ERROR, "error happened"))
            logger.error(e)
        channel = grpc.insecure_channel("{}".format(address+":"+port))
        stub = tokenomics_pb2_grpc.TokenomicsStub(channel)
        txn_hash = req.txn_hash
        result = stub.getResult(tokenomics_pb2.TxnHash(txn_hash=txn_hash))
        current_span.add_event("event message", {"result": str(result)})
        return result

    async def makeAdapterTransaction(self, req, ctxt):
        current_span = trace.get_current_span()
        current_span.set_attribute("http.route", "some_route")
        self.agi_queue.append(req)
        return nunet_adapter_pb2.AdapterReleaseFundsOutput(response=req.public_key)

    async def reqPrice(self, req, ctxt):
        service_name = req.service_name
        tracer_info = eval(req.tracer_info)

        trace_id = tracer_info['trace_id']
        span_id = tracer_info['span_id']

        span_context = SpanContext(
        trace_id=trace_id,
        span_id=span_id,
        is_remote=True,
        trace_flags=TraceFlags(0x01)
        )
        ctx = trace.set_span_in_context(NonRecordingSpan(span_context))
        with tracer.start_as_current_span("reqPrice", context=ctx) as span:
            trace_info=span.get_span_context()
            span_id = trace_info.span_id
            trace_id = trace_info.trace_id

            try:
                req_address = utils.get_service_address(SharedRegistry.service_map,service_name)
                address = req_address[0]
                port = req_address[1]
                logger.info(address)
                logger.info(port)
            except Exception as e:
                logger.error(e)

            serviceAddress = address+":"+port
            temp=0
            cnt=0
            
            while temp==0 and cnt<10:
                try:
                    channel = grpc.insecure_channel("{}".format(serviceAddress))
                    stub = service_proto_pb2_grpc.ProtoDefnitionStub(channel)
                    temp=1
                except Exception as e:
                    try:
                        req_address = utils.get_service_address(SharedRegistry.service_map,service_name)
                        address = req_address[0]
                        port = req_address[1]
                        logger.info(address)
                        logger.info(port)
                        serviceAddress = address+":"+port
                    except Exception as e:
                        logger.error(e)
                    
                cnt+=1

            metaParams = service_proto_pb2.metaParams()
            metaParams.service_name = service_name
            metaParams.tracer_info = str({
                        'trace_id': trace_id,
                        'span_id': span_id
                    })
            loop = asyncio.get_event_loop()
            res = await loop.run_in_executor(None,stub.req_metadata,metaParams)
            # res = stub.req_metadata(metaParams)
            logger.info(res)
            service_definition=eval(res.service_definition)
            logger.info(service_definition)
            #### get providers public key from machine metadata
            try:
                with open('/etc/nunet/metadata.json', 'r') as file:
                    metadata = file.read()
                pubk=eval(metadata)["public_key"]  
            except Exception as e:
                logger.error(e)
                pubk="0xb5114121A51c6FfA04dBC73F26eDb7B6bfE2eB35"
            cost_per_process=service_definition["ntx_price"]
            priceResp = nunet_adapter_pb2.priceResp()
            priceResp.cost_per_process = cost_per_process
            priceResp.pubk = pubk
            return priceResp

    async def getProviderPaymentParams(self, req, ctxt):

        service_name = req.service_name
        tracer_info = eval(req.tracer_info)
        
        trace_id = tracer_info['trace_id']
        span_id = tracer_info['span_id']
        service_id=str(service_name)+str(trace_id)
        logger.info(service_id)
        span_context = SpanContext(
        trace_id=trace_id,
        span_id=span_id,
        is_remote=True,
        trace_flags=TraceFlags(0x01)
        )
        ctx = trace.set_span_in_context(NonRecordingSpan(span_context))
        with tracer.start_as_current_span("getProviderPaymentParams", context=ctx) as span:
            
            trace_info=span.get_span_context()
            span_id = trace_info.span_id
            trace_id = trace_info.trace_id
            uid = uuid.uuid1()

            try:
                endpoints = SharedRegistry.getServiceEndpoints(service_name)
                if not endpoints:
                    logger.info("No endpoints found for service {} in internal registry ".format(service_name))
                    endpoints = utils.get_endpoints(service_name)
                
    
                peer_ids = endpoints["peer_infos"]
                ip_addrs = endpoints["endpoints"]
                i=0
                for peer_info in peer_ids:    
                    msg={}
                    msg['peer_address'] = peer_info['_address']
                    msg['public_key'] = peer_info['public_key']
                    msg['service_name'] = service_name

                    msg["uid"] = uid
                    encoded_result = utils.encode_message(msg)
                    key = LibNaCLPK(peer_info['key'])
                    peer = Peer(key)
                    peer.mid = peer_info['mid']
                    peer.public_key = peer_info['public_key']
                    peer._address = peer_info['_address']
                    address = ip_addrs[i][0]
                    port = ip_addrs[i][1]
                    i+=1
                    self.health_check_result[uid] = None
                    self.ez_send(peer,ipv8_messages.HealthCheckReceiver(encoded_result))
                    logger.info(address)
                    logger.info(port)

                timeout = 50
                start_time = time.time()
                logger.info(peer_info)
                while self.health_check_result[uid] == None:
                    if time.time() - start_time > timeout:
                        break
                    await asyncio.sleep(1/1000)
                
                logger.info(self.health_check_result[uid])
                
               
                
                logger.info(self.health_check_result[uid]["peer_address"])
                logger.info(self.health_check_result[uid]["public_key"])
                for peer_info in peer_ids:
                    key = LibNaCLPK(peer_info['key'])
                    peer = Peer(key)
                    peer.mid = peer_info['mid']
                    peer.public_key = peer_info['public_key']
                    peer._address = peer_info['_address']
                    if self.health_check_result[uid]["public_key"] == peer_info["public_key"]:
                        self.checked_peer_infos[service_id]=peer_info["public_key"]
                        logger.info(self.checked_peer_infos[service_id])
                        logger.info(peer_info["_address"])
                        key = LibNaCLPK(peer_info['key'])
                        peer_2 = Peer(key)
                        peer_2.mid = peer_info['mid']
                        peer_2.public_key = peer_info['public_key']
                        peer_2._address = peer_info['_address']
                logger.info(address)
                peer=peer_2

            except Exception as e:
                exception = Exception(str(e))
                span.record_exception(exception)
                span.set_status(Status(StatusCode.ERROR, "error happened"))
                logger.error(e)


            try:
                self.req_price_result[uid] = None
                msg = {}
                msg["service_name"] = service_name
                msg["uid"] = uid
                msg["tracer_info"] = {
                    'trace_id': trace_id,
                    'span_id': span_id
                }
                timeout = 50
                encoded_msg = utils.encode_message(msg)
                self.ez_send(peer,ipv8_messages.ReqPriceMsg(encoded_msg))
                logger.info("Sent .......")
            except:
                logger.info(cost_per_process)
                logger.info(pubk)

            while self.req_price_result[uid] == None:
                if time.time() - start_time > timeout:
                    break
                await asyncio.sleep(1/1000)

            logger.info(self.req_price_result[uid])
            result = self.req_price_result[uid]
            pubk=result["public_key"]
            cost_per_process=result["cost_per_process"]        
            
            fp = nunet_adapter_pb2.AdapterMachineProviderOutput()
            fp.cost_per_process = str(cost_per_process)
            fp.pubk = pubk
            logger.info("Returning from getPaymentProvider")
            logger.info(self.req_price_result[uid])

            return fp

    async def reqAdapterMetadata(self, req, ctxt):
        try:
            # req = next(request_iterator)
            logger.info("Received request from serivce")
        except Exception as e:
            logger.error(e)
        uid = uuid.uuid1()
        self.req_adapter_metadata_result[uid] = None
        service_name = req.service_name

        try:

            endpoints = SharedRegistry.getServiceEndpoints(service_name)
            logger.info(endpoints)
            if not endpoints:
                logger.info("No endpoints found for service {} in internal registry ".format(service_name))
                endpoints = utils.get_endpoints(service_name)
            self.health_check_result[uid] = None
            peer_ids = endpoints["peer_infos"]
            ip_addrs = endpoints["endpoints"]
            i=0
            for peer_info in peer_ids:
                msg={}
                msg['service_name']= service_name
                msg['peer_address'] = peer_info['_address']
                msg['public_key'] = peer_info['public_key']
                msg['uid'] = uid
                encoded_result = utils.encode_message(msg)
                key = LibNaCLPK(peer_info['key'])
                peer = Peer(key)
                peer.mid = peer_info['mid']
                peer.public_key = peer_info['public_key']
                peer._address = peer_info['_address']
                address = ip_addrs[i][0]
                port = ip_addrs[i][1]
                i+=1
                

                self.ez_send(peer,ipv8_messages.HealthCheckReceiver(encoded_result))
                logger.info(address)
                logger.info(port)
            timeout = 10    
            start_time = time.time()
                
            while self.health_check_result[uid] == None:
                if time.time() - start_time > timeout:
                    break     
                await asyncio.sleep(1/1000)
                
            logger.info(self.health_check_result[uid])
            

            # logger.info(self.health_check_result[uid]["peer_address"])
            # logger.info(self.health_check_result[uid]["public_key"])
            for peer_info in peer_ids:
                key = LibNaCLPK(peer_info['key'])
                peer = Peer(key)
                peer.mid = peer_info['mid']
                peer.public_key = peer_info['public_key']
                peer._address = peer_info['_address']
                if self.health_check_result[uid]["public_key"] == peer_info["public_key"]:
                    logger.info(peer_info["_address"])
                    key = LibNaCLPK(peer_info['key'])
                    peer_2 = Peer(key)
                    peer_2.mid = peer_info['mid']
                    peer_2.public_key = peer_info['public_key']
                    peer_2._address = peer_info['_address']
            peer=peer_2

        except Exception as e:
            logger.error(e)

        msg = {}
        msg["service_name"] = service_name
        msg["uid"] = uid
        encoded_msg = utils.encode_message(msg)
        self.ez_send(peer,ipv8_messages.reqAdapterMetadataMsg(encoded_msg))

        start_time = time.time()
        timeout = 13
        ipv8 = True

        while self.req_adapter_metadata_result[uid] == None:
            if time.time() > start_time + timeout:
                ipv8 = False
                break
            await asyncio.sleep(1/1000)


        if not ipv8:
            logger.info("IPv8 connection timed out trying gRPC")
            adapterAddress = address+":"+port
            async with grpc.aio.insecure_channel("{}".format(adapterAddress)) as channel:

                stub = nunet_adapter_pb2_grpc.NunetAdapterStub(channel)
                fp = nunet_adapter_pb2.metaServiceParams()
                fp.service_name = service_name
                result = await stub.reqServiceMetadata(fp)
            logger.info(result.service_definition)
            resp = nunet_adapter_pb2.respAdapterMetadata()
            resp.service_definition = result.service_definition
            return resp


        resp = nunet_adapter_pb2.respAdapterMetadata()
        resp.service_definition = self.req_adapter_metadata_result[uid].service_definition    
        return resp

    async def reqServiceMetadata(self, req, ctxt):
        service_name = req.service_name
        try:
            req_address = utils.get_service_address(SharedRegistry.service_map,service_name)
            address = req_address[0]
            port = req_address[1]
            logger.info(address)
            logger.info(port)
        except Exception as e:
            logger.error(e)

        serviceAddress = address+":"+port
        temp=0
        cnt=0
        
        while temp==0 and cnt<10:
            try:
                channel = grpc.insecure_channel("{}".format(serviceAddress))
                stub = service_proto_pb2_grpc.ProtoDefnitionStub(channel)
                temp=1
            except Exception as e:
                try:
                    req_address = utils.get_service_address(SharedRegistry.service_map,service_name)
                    address = req_address[0]
                    port = req_address[1]
                    logger.info(address)
                    logger.info(port)
                    serviceAddress = address+":"+port
                except Exception as e:
                    logger.error(e)
                
            cnt+=1

        metaParams = service_proto_pb2.metaParams()
        metaParams.service_name = service_name
        metaParams.tracer_info = str({
            'trace_id': 1,
            'span_id': 1
        })
        res = stub.req_metadata(metaParams)
        logger.info("Returned result from service")
        return nunet_adapter_pb2.respServiceMetadata(service_definition=res.service_definition)

    async def reqServiceWorkflow(self, req, ctxt):
        current_span = trace.get_current_span()
        current_span.set_attribute("http.route", "some_route")
        sleep(30 / 1000)

        tracer_info = req.tracer_info
        tracer_info = eval(tracer_info)

        span_context = SpanContext(
        trace_id=tracer_info['trace_id'],
        span_id=tracer_info['span_id'],
        is_remote=True,
        trace_flags=TraceFlags(0x01)
        )

        ctx = trace.set_span_in_context(NonRecordingSpan(span_context))
        with tracer.start_as_current_span("reqServiceWorkflow",context = ctx) as span:
            
            trace_info=span.get_span_context()
            span_id = trace_info.span_id
            trace_id = trace_info.trace_id
        
            service_name = req.service_name
            params = eval(req.params)
            params['tracer_info'] = str({
                'trace_id': trace_id,
                'span_id': span_id
            })
            params = str(params)
            declarations = json.loads(req.declarations)
            service_input_params=req.service_input_params
            
            try:
                req_address = utils.get_service_address(SharedRegistry.service_map,service_name)
                address = req_address[0]
                port = req_address[1]
                logger.info(address)
                logger.info(port)
            except Exception as e:
                exception = Exception(str(e))
                span.record_exception(exception)
                span.set_status(Status(StatusCode.ERROR, "error happened"))
                logger.error(e)
            span.add_event("event message", {"service name": service_name})
            sleep(30 / 1000)
            span.add_event("event message", {"service declaration": str(declarations)})

            proto_defnition = declarations["protobuf_definition"]
            function_name = declarations["function"]
            service_input = declarations["input"]
            service_stub = declarations["service_stub"]

            proto_file_name = "proto_"+service_name+".proto"
            f = open(proto_file_name, "w")
            f.write(proto_defnition)
            f.close()
            logger.info(str(proto_defnition))

            service_input_params=eval(service_input_params)
            inputs=""
            logger.info(str(service_input_params))
            i=0
            p=ast.literal_eval(params)
            for param in service_input_params:
                logger.info(str(param))
                logger.info(p[param])
                if param in list(ast.literal_eval(params).keys()):
                    value=p[param]
                    value=value.replace("\'","\\\'").replace("\"","\\\"")
                    value=value.replace("\n"," ").replace("\t"," ")
                    value=value.replace("\b"," ").replace("\r"," ")
                    value=value.replace("\f"," ")
                    if i==0:
                        logger.info(str(i))
                        inputs+=param+"="+'"'+value+'"'
                    else:
                        inputs+=", "+param+"="+'"'+value+'"'
                    i=1
            logger.info(str(inputs))

            subprocess.call(["python3", "-m", "grpc_tools.protoc",
                            "-I.", "--python_out=.", "--grpc_python_out=.",
                            proto_file_name])

            service_name = service_name.replace("-", "_")
            proto_service_pb2 = "proto_"+service_name+"_pb2"
            proto_service_pb2_grpc = "proto_"+service_name+"_pb2_grpc"
            proto_pb2 = __import__(proto_service_pb2)
            proto_pb2_grpc = __import__(proto_service_pb2_grpc)

            serviceAddress = address+":"+port
            span.add_event("event message", {"service address": str(serviceAddress)})
            temp=0
            cnt=0
            
            while temp==0 and cnt<10:
                try:
                    channel = grpc.insecure_channel("{}".format(serviceAddress))
                    stub = getattr(proto_pb2_grpc, service_stub)(channel)
                    temp=1
                except Exception as e:
                    try:
                        req_address = utils.get_service_address(SharedRegistry.service_map,service_name)
                        address = req_address[0]
                        port = req_address[1]
                        logger.info(address)
                        logger.info(port)
                        serviceAddress = address+":"+port
                    except Exception as e:
                        logger.error(e)
                    
                cnt+=1
            
            param_string="getattr(proto_pb2, service_input)("+inputs+")"
            logger.info(param_string)
            inp=eval(param_string)
            logger.info(inp)
            logger.info("Calling service using gRPC")
            loop = asyncio.get_event_loop()
            result = await loop.run_in_executor(None,getattr(stub, function_name),inp)
            result = google.protobuf.json_format.MessageToJson(result)
            span.add_event("event message", {"service result": str(result)})
            sleep(30 / 1000)
            logger.info(result)
            return nunet_adapter_pb2.ServiceResponse(service_response=result)

    def agi_transfer(self):
        while True:
            try:
                if self.agi_queue !=[]:
                    logger.info(self.agi_queue)
                req=self.agi_queue.pop(0)
                public_key = req.public_key
                amount = req.agi_amount
                escrow_address=req.escrow_address
                logger.info(escrow_address)
                call_id=eval(escrow_address)["call_id"]
                service_name=eval(escrow_address)["service"]
                #logger.info(call_id)
                #logger.info(service_name)
                txn_hash = snet_utils.agi_transfer(public_key, amount,call_id, service_name)
                logger.info(txn_hash)        
            except:
                pass
            time.sleep(0.000001)

    async def getPeerList(self, req, ctxt):
        known_peers=[]
        for peer in self.get_peers():
            logger.info(peer.mid)
            known_peers.append(peer.mid)
        return nunet_adapter_pb2.peers(peer_address=str(known_peers))
    
    async def getSelfPeer(self, req, ctxt):
        return nunet_adapter_pb2.nodeID(node_id=str(SharedRegistry.nodeID))


    async def getDhtContent(self, req, ctxt):
        logger.info(SharedRegistry.machines_index)
        logger.info(SharedRegistry.services_index)
        dhtContent = nunet_adapter_pb2.DhtContents()
        dhtContent.machines_index = json.dumps(SharedRegistry.machines_index, cls=HexBytesEncoder)
        dhtContent.available_resources_index = json.dumps(SharedRegistry.available_resources_index, cls=HexBytesEncoder)
        dhtContent.services_index = json.dumps(SharedRegistry.services_index, cls=HexBytesEncoder)
        
        return dhtContent

    async def updateDHT(self, req, ctxt):
        """
        Updates the contents of the DHT. Allows updating a single attribute of the DHT.
        """
        logger.info(f"Allow cardano {req.allow_cardano}")
        logger.info(f"Available Resources {req.available_resources}")
        allow_cardano = req.allow_cardano
        has_gpu = req.has_gpu
        gpu_info = req.gpu_info
        services_index = req.services_index
        available_resources = req.available_resources
        if allow_cardano:
            SharedRegistry.allow_cardano_deployment = allow_cardano
        if has_gpu:
            SharedRegistry.has_gpu = has_gpu
        if gpu_info:
            gpu_info = json.loads(gpu_info)
            SharedRegistry.gpu_info = gpu_info
        if services_index and SharedRegistry.nodeID:
            services_index = json.loads(services_index)
            SharedRegistry.services_index[SharedRegistry.nodeID] = services_index
        if available_resources and SharedRegistry.nodeID:
            available_resources = json.loads(available_resources)
            SharedRegistry.available_resources_index[SharedRegistry.nodeID] = available_resources
            SharedRegistry.machines_index[SharedRegistry.nodeID]["available_resources"] = available_resources
            logger.info(SharedRegistry.machines_index[SharedRegistry.nodeID]["available_resources"])
        return nunet_adapter_pb2.DHTUpdateResponse(response="DHT updated")



    async def sendMessage(self, req, ctxt):
        node_id=req.node_id

        peer_info = SharedRegistry.machines_index[str(node_id)]["peer_info"]
        key = LibNaCLPK(peer_info["key"])
        peer = Peer(key)
        peer.mid = peer_info["mid"]
        peer._address = peer_info["_address"]
        peer.public_key = peer_info["public_key"]

        message_content=req.message_content
        msg = {}
        msg["message"] = message_content
        msg["uid"] = str(uuid.uuid1())
        msg["msg_time"] = datetime.datetime.now(datetime.timezone.utc).astimezone().isoformat()
        msg["sender"] = str(SharedRegistry.nodeID)
        logger.info(f"********Sending message: {msg}")
        encoded_msg = utils.encode_message(msg)
        self.ez_send(peer,ipv8_messages.SendMessage(encoded_msg))
        
        return nunet_adapter_pb2.messageResponse(message_response="sent")


    async def receivedMessage(self, req, ctxt):        
        return nunet_adapter_pb2.messageResponse(message_response=str(self.message_queue))
    
    async def executeCommand(self, req, ctxt):
        command = req.command
        if command == "start":
            ShellSession.started = True
            return nunet_adapter_pb2.Response(response="started")
       
        if ShellSession.command == None:
            ShellSession.command = command
        
        
        while not ShellSession.responded:
            # Wait until response is returned from MasterCommunity
            await asyncio.sleep(2)
        
       
        # Extract response and set response field back to None
        response = ShellSession.response
        ShellSession.response = None
        ShellSession.responded = False
        return nunet_adapter_pb2.Response(response=response)

    async def getMasterPkey(self, req, ctxt):
        if ShellSession.master_pkey:
            pkey = binascii.hexlify(ShellSession.master_pkey).decode('utf-8')
            return nunet_adapter_pb2.Pkey(public_key=pkey)


    def incomingMessage(self, req, ctxt):
        while True:
            if len(self.message_queue) > 0:
                received_msg = self.message_queue.pop(0)
                logger.info(f"MSG: {len(self.message_queue)} messages in que")
                yield nunet_adapter_pb2.messageResponse(message_response=str(received_msg))
            time.sleep(1)


async def run_grpc_server(class_name):
    # server = grpc.server(futures.ThreadPoolExecutor(max_workers=1000))
    # server = grpc.aio.server(migration_thread_pool=futures.ThreadPoolExecutor(max_workers=1000))
    server = grpc.aio.server(options = (
        ('grpc.so_reuseport', 0),
        ('grpc.keepalive_time_ms', 30000), # 30 seconds
        ('grpc.keepalive_timeout_ms', 60000), # 60 seconds
        ('grpc.keepalive_permit_without_calls', 1), # allow keepalive ping even without data
        ('grpc.http2.max_pings_without_data', 0) # disable limit on number of pings
    ))
    nunet_adapter_pb2_grpc.add_NunetAdapterServicer_to_server(
        class_name, server)
    server.add_insecure_port('[::]:'+str(grpc_port))
    await server.start()
    logger.info("Server listening on 0.0.0.0:{}".format(grpc_port))
    try:
        while True:
            await asyncio.sleep(10)
    except KeyboardInterrupt:
        await server.stop(0)
    

async def main():
    logger.info("Detecting NAT...")
    subprocess.call(["python2", "natdetector.py", "save"])
    with open('./nat-type.json', 'r') as file:
        meta = json.load(file)
        nat_type = meta['nat_type']
        logger.info(f"**NAT type:{nat_type} ")
    session = Session(nat_type=nat_type)
    await session.start()

    nunetadapter = session.nunet_community
    logger.info(nunetadapter)
    session.discoverpeer_community.dht_update()
    get_event_loop().create_task(run_grpc_server(nunetadapter))
    new_loop = asyncio.new_event_loop()
    #new_loop.run_in_executor(None,nunetadapter.agi_transfer)
    new_loop.run_in_executor(None,nunetadapter.incomingMessage)

if __name__ == '__main__':
    ensure_future(main())
    get_event_loop().run_forever()

